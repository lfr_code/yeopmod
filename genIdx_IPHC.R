# --------------------------------------------
# Calculate summary stats and indexes for Yelloweye catch from IPHC survey data from 1998-2017

# Author: B Doherty, Landmark Fisheries Research
# Last Edit: Aug 2019

# --------------------------------------------

# Clear R Space
# rm(list = ls())

setwd('~/Documents/LANDMARK/PHMA/2018_YEassessment/yeopmod')

source('GIStools.R')
source('YEfuns.r')

# load packages
library(sp)
library(dplyr)

# read in 1998 - 2015 survey data
catchDat <- read.csv('data/survey/iphcCatchInfo1998_2015.csv')
haulDat <- read.csv('data/survey/iphcHaulInfo1998_2015.csv')

# read in survey data for 2003-2018
ll <- read.csv('data/survey/longlineSurveyCatch.csv')

# load in shapefile for major areas
mjr <- shapefile('GISdata/DFO_MajorArea.shp')

# ye -  YE #s for first 20 hooks (N_it20) and all hooks (N_it)
ye <-  readRDS("data/survey/yelloweye-rockfish.rds")
ye <- ye$set_counts
ye <- as.data.frame(ye)

# ---- load in hook competition adjustments ---- #
haf <- read.csv('hookComp/HAFs_2003_2017.csv')
haf$mgmtArea <- as.character(haf$mgmtArea)

###########################################
# --- Process data and merge datasets ---
###########################################

# Remove 1995-1997 stations & 'unusable sets'
ye <- subset(ye, year>=1998 & usable=='Y')

# assign column for YE presence
ye$pres <- 0
rows <- which(ye$C_it >0 | ye$C_it20 >0)
ye$pres[rows] <- 1

# add best CPUE col
ye$cpue <- ye$C_it
rows <- which(is.na(ye$cpue))
ye$cpue[rows] <- ye$C_it20[rows]

# Check ratio of effective skates to hooks.Retrieve
haulDat$hksRetr_effSkateRatio <- haulDat$Hooks.Retrieved/haulDat$Effective.Skates
hist(haulDat$hksRetr_effSkateRatio)
tmp <- subset(haulDat,hksRetr_effSkateRatio<99)


ll$DATE <- as.Date(ll$DATE, format='%y-%m-%d') 
ll$year <- ll$YEAR

# Replace NA counts with zeros
rows <- which(is.na(ll$YELLOWEYE_PCS))
ll$YELLOWEYE_PCS[rows] = 0

# subset for IPHC
ll <- subset(ll, ACTIVITY_CODE ==24)

# Add YE catch to events
dat1 <- dplyr::left_join(haulDat, 
                        catchDat[,c('Year','Station','Number.Observed')],
                        by=c('Year','Station'))

# Calc CPUE and remove rows without any hooks observed
dat1 <- subset(dat1, Hooks.Observed >0)
dat1$Number.Observed[is.na(dat1$Number.Observed)] <-0
dat1$cpue <- dat1$Number.Observed/dat1$Hooks.Observed*100


# Merge datasets Add 2016-2018 data
dat1$percObs <- dat1$Hooks.Observed/dat1$Hooks.Retrieved
dat1 <- dat1[,c('Year','Station','MidLat','MidLon',
               'Hooks.Observed','Number.Observed','percObs')]
dat2 <- ll[,c('YEAR','IPHC_STATION','LATITUDE','LONGITUDE',
               'HOOKS_FISHED', 'YELLOWEYE_PCS')]
dat2$percObs <- 1
names(dat2) <- names(dat1)
dat2 <- subset(dat2, Year %in% c(2016,2017,2018))
dat <- rbind(dat1, dat2)

dat$cpue <- dat$Number.Observed/dat$Hooks.Observed*100

################################
# --- Create Summary Tables ---
################################

# Summary for all stations
t <- dplyr::summarise(group_by(dat, Year),
          stns = length(unique(Station)), 
          meanHooksObs_1000s = mean(Hooks.Observed),
          meanPercObs = mean(percObs),
          nNonZeros = length(Station[Number.Observed>0]),
          propNonZero =nNonZeros/stns
          )

write.csv(t,'tables/iphcHookCountSummary_all2018Stns.csv', row.names=F)

# Summary exlcuding new 2018 stations
sub.dat <- subset(dat, Station <2200)
t2 <-  dplyr::summarise(group_by(sub.dat,Year),
          stns = length(unique(Station)), 
          meanHooksObs_1000s = mean(Hooks.Observed),
          meanPercObs = mean(percObs),
          nNonZeros = length(Station[Number.Observed>0]),
          propNonZero =nNonZeros/stns
          )

write.csv(t2,'tables/iphcHookCountSummary_exclNew2018Stns.csv', row.names=F)

# Create unique stn id
# *** code below this uses summary to compare stations with YE presence on first 20 hooks vs all hooks - this can be updated once 2018 dataset is obtained from A. Edwards
names(dat)[names(dat)=='Station'] <- 'station'
names(dat)[names(dat)=='Year'] <- 'year'
stnInfo <- dplyr::summarise(group_by(dat, station),
          yrsFished = length(cpue),
          yrsYEpres = length(cpue[cpue > 0]),
          firstYr = min(year),
          midLon = mean(MidLon),
          midLat = mean(MidLat),
          meanCPUE = mean(cpue)
          )

# Generate summary tables for 1998-2017, with catch for full set and first 20 hooks only
# stnInfo <- dplyr::summarise(group_by(ye, station),
#           yrsFishedUsable = length(usable[usable=='Y']),
#           yrsFished = length(cpue),
#           yrsYEpres = length(pres[pres==1]),
#           yrsYEpres20 = length(N_it20[N_it20 > 0]),
#           firstYr = min(year),
#           midLon = mean(lon),
#           midLat = mean(lat),
#           meanCPUE = mean(cpue)
#           )

# Assign Area for stations
stnPts <- as.matrix(stnInfo[c('midLon', 'midLat')])
area <- assignArea(stnPts, mjr)
stnInfo$mjr <- area$label
stnInfo <- as.data.frame(stnInfo)

# Assign stations to north or south index
stnInfo$mainIdx <- NA
stnInfo[stnInfo$mjr %in% c('5E','5D','5C','5B'),]$mainIdx <- 'northIdx'
stnInfo[stnInfo$mjr %in% c('5A','3D','3C'),]$mainIdx <- 'southIdx'


# Assign sub indices
stnInfo$subIdx <- NA

rows <- which(stnInfo$mainIdx=='northIdx')
stnInfo$subIdx[rows] <- '5BCDE'

rows <- which(stnInfo$firstYr!=1998 & stnInfo$mainIdx=='southIdx')
stnInfo$subIdx[rows] <- 's5A3CD'

rows <- which(stnInfo$firstYr==1998 & stnInfo$mainIdx=='southIdx')
stnInfo$subIdx[rows] <- 'n5A'

stnKey <- data.frame(subIdx =c('5BCDE','s5A3CD','n5A'),
                     colr=c('#d95f02','#1b9e77','#7570b3')
                         )

stnInfo <- dplyr::left_join(stnInfo, stnKey, by='subIdx')
stnInfo$colr <- as.character(stnInfo$colr)

write.csv(stnInfo,'tables/iphcStnSummary.csv', row.names=F)

# Create summary table for threshold 1
idxStn <- subset(stnInfo, yrsYEpres>=1 & yrsFished>10)
idxStn$propYrsYE <- idxStn$yrsYEpres/idxStn$yrsFished 
idxSmry <- dplyr::summarise(group_by(idxStn, mainIdx, mjr),
                            nStations    = length(propYrsYE),
                            minPropYrsYe = min(propYrsYE),
                            meanPropYrsYe = mean(propYrsYE),
                            minYrsYE      = min(yrsYEpres),
                            meanYrsYE     = mean(yrsYEpres)
                            )

write.csv(idxSmry, 'tables/indexStationStats_thresh1.csv', row.names=F)

# Create summary table for threshold 11
idxStn <- subset(stnInfo, yrsYEpres>=11)
idxStn$propYrsYE <- idxStn$yrsYEpres/idxStn$yrsFished 
idxSmry <- dplyr::summarise(group_by(idxStn, mainIdx, mjr),
                            nStations    = length(propYrsYE),
                            minPropYrsYe = min(propYrsYE),
                            meanPropYrsYe = mean(propYrsYE),
                            minYrsYE      = min(yrsYEpres),
                            meanYrsYE     = mean(yrsYEpres)
                            )

write.csv(idxSmry, 'tables/indexStationStats_thresh11.csv', row.names=F)



#####################################
# --- Map Stock Area ---
#####################################

# Save using quartz.save() as pdf() doesn't like arrows symbols.
stockAreaMap()
quartz.save(file='Outputs/plots/stockAreaMap.pdf', type = "pdf")


stockAreaMap(stockLabel=FALSE)
quartz.save(file='Outputs/plots/stockAreaMap_NoStockLabels.pdf', type = "pdf")

stockAreaMap(stockLabel=FALSE)
quartz.save(file='Outputs/plots/stockAreaMap_NoStockLabels.png', type = "png")

#####################################
# --- Map IPHC stations for index ---
#####################################

# Thresh=1
idxStnInfo <- subset(stnInfo, yrsFished>1)

iphcStnMap(idxStnInfo, plotBinCPUE=F, thresh=1)

legend('topright', title= 'IPHC Sations',bg='white',
       legend= c('North Index Stations (1998-2018)',
                 'South Index Stations (1998-2018)',
                 'South Index Stations (1999, 2001-2018)'),
     pch=19, col=c('#d95f02','#7570b3','#1b9e77'))

quartz.save('Outputs/plots/iphcStnMap_thresh1.png', type='png')

graphics.off()

# Thresh=11
iphcStnMap(idxStnInfo, plotBinCPUE=F, thresh=11)

legend('topright', title= 'IPHC Sations',bg='white',
       legend= c('North Index Stations (1998-2018)',
                 'South Index Stations (1998-2018)',
                 'South Index Stations (1999, 2001-2018)'),
     pch=19, col=c('#d95f02','#7570b3','#1b9e77'))

quartz.save('Outputs/plots/iphcStnMap_thresh11.png', type='png')

graphics.off()

############################
# --- Calculate Indices ---
############################

# Using index provided by A. Edwards for 1998-2018
ye$station <- as.integer(ye$station)
idxDat <- ye[,c('year','station','pres','cpue')]

# *** add 2018 data, this can be removed once updated dataset received from A. Edwards with 2018 IPHC dat
# dat$pres <-  dat$Number.Observed
# dat$pres[dat$pres>0] <- 1
# dat18 <- dat[,c('year','station','pres','cpue')]
# dat18 <- subset(dat18, year==2018)

# idxDat <- rbind(idxDat,dat18)

dat <-dplyr::left_join(idxDat, 
             stnInfo[,c('station','firstYr','mjr','mainIdx', 'subIdx',
               'yrsYEpres','yrsFished')])

nzDat <- subset(dat, yrsYEpres>0 & yrsFished>1)


# subset data for different regions

# northern index
n1 <- subset(nzDat, mainIdx=='northIdx')

# southern index
s1 <- subset(nzDat, mainIdx=='southIdx'
              & !(year %in% c(1998, 2000)))

# southern index for years 1999, 2000-2017
s5A3CD <- subset(nzDat, subIdx=='s5A3CD' )

# southern index for years 1998-2017
n5A <- subset(nzDat, subIdx=='n5A')

# coastwide index
cw <- subset(nzDat, !(year %in% c(1998, 2000)))



# --- Calculate IPHC index without hook comp ---

# Generate indices for different thresholds for excluding stations with YE catch in limited years

dataset <- list(n1,s1,s5A3CD, n5A, cw)
iphcIdx_ysit <- genIPHCidx(dataset)
save(iphcIdx_ysit, file='Data/iphcIdx_ysit.Rdata')


# Plot indices
iphcIdxPlot(iphcIdx_ysit[,,1,c(1,5,8,11)])
legend('bottomright',legend='North Index (5BCDE)', bty='n')
quartz.save('Outputs/plots/iphcIdx_5BCDE.png',type='png')

iphcIdxPlot(iphcIdx_ysit[,,1,c(1,3,5,7,9,11)], addIdxAB=TRUE)
# legend('bottomright',legend='North Index (5BCDE)', bty='n')
quartz.save('Outputs/plots/iphcIdx_5BCDEwithIdxAB.png',type='png')

iphcIdxPlot(iphcIdx_ysit[,,2,c(1,3,5,7,9,11)])
# legend('bottomright',legend='South Index (5A3CD)', bty='n')
quartz.save('Outputs/plots/iphcIdx_5A3CD.png',type='png')

iphcIdxPlot(iphcIdx_ysit[,,3,c(1,5,8,11)])
legend('bottomright',legend='South Index Subset (s5A3CD)', bty='n')
quartz.save('Outputs/plots/iphcIdx_s5A3CD.png',type='png')

iphcIdxPlot(iphcIdx_ysit[,,4,c(1,5,8,11)])
legend('bottomright',legend='South Index Subset (n5A)', bty='n')
quartz.save('Outputs/plots/iphcIdx_n5A.png',type='png')

iphcIdxPlot(iphcIdx_ysit[,,5,c(1,5,8,11)])
legend('bottomright',legend='Coastwide Index', bty='n')
quartz.save('Outputs/plots/iphcIdx_coastwide.png',type='png')

graphics.off()


# Creat tables summarizing mean CVs for different thresholds

cvTab <- iphcIdxCVs(iphcIdx_ysit)

# Calculate CVs as standard error divided by the mean
write.csv(cvTab$relERR, 'Tables/iphcIdxCVs_relERR.csv', row.names=F)

# Calculate CVs as standard deviation divided by the mean
write.csv(cvTab$relSD, 'Tables/iphcIdxCVs_relSD.csv', row.names=F)



# --- Calculate IPHC index with hook comp ---

names(haf)[names(haf)=='Year'] <- 'year'
names(haf)[names(haf)=='mgmtArea'] <- 'mjr'
hafYrs <- unique(haf$year) 
# hook by hook data unavailable for 1998-2002 & 2013

# northern index
n1 <- subset(nzDat, mainIdx=='northIdx' 
              & year %in% hafYrs)
n1.haf<- subset(haf, surveyType=='iphcNorth')
n1 <- dplyr::left_join(n1, n1.haf[,c('year','mjr', 'HAF','HAFw')], by=c('year','mjr'))


# southern index
s1 <- subset(nzDat, mainIdx=='southIdx'
              & !(year %in% c(1998, 2000))
              & year %in% hafYrs)
s1.haf<- subset(haf, surveyType=='iphcSouth')
s1 <- dplyr::left_join(s1, s1.haf[,c('year','mjr', 'HAF','HAFw')], by=c('year','mjr'))

# non-weighted hook comp
s1HAF <- s1
n1HAF <- n1
n1HAF$cpue <- n1HAF$cpue *n1HAF$HAF
s1HAF$cpue <- s1HAF$cpue *s1HAF$HAF
dataset <- list(n1HAF,s1HAF)
iphcIdx_ysitHAF <- genIPHCidx(dataset, 
                              idxArea=c('5BCDE','5A3CD'))
save(iphcIdx_ysitHAF, file='Data/iphcIdx_ysitHAF.Rdata')

# weighted hook comp
s1HAFw <- s1
n1HAFw <- n1
n1HAFw$cpue <- n1HAFw$cpue *n1HAFw$HAFw
s1HAFw$cpue <- s1HAFw$cpue *s1HAFw$HAFw
dataset <- list(n1HAFw,s1HAFw)
iphcIdx_ysitHAFw <- genIPHCidx(dataset, 
                              idxArea=c('5BCDE','5A3CD'))
save(iphcIdx_ysitHAFw, file='Data/iphcIdx_ysitHAFw.Rdata')


# Compare indices for thresh 5
thr <- 11
It.list <- list(north     = iphcIdx_ysit [,,1,thr],
                northHAF  = iphcIdx_ysitHAF [,,1,thr],
                northHAFw = iphcIdx_ysitHAFw [,,1,thr],
                south     = iphcIdx_ysit [,,2,thr],
                southHAF  = iphcIdx_ysitHAF [,,2,thr],
                southHAFw = iphcIdx_ysitHAFw [,,2,thr] )

plotname <- paste('Outputs/plots/iphcIdx_thr',
                  thr,'.png',sep='')
png(filename=plotname,
    width=800, height=600, res=100)
plotIPHCidx_HAF (It.list, thresh=thr)
dev.off()

# --- Fit lm for predicting southIdx in 1998 and 2000 ---

library(ggfortify)


s.idx <- as.data.frame(iphcIdx_ysit[,,2,thr])
n5.idx <- as.data.frame(iphcIdx_ysit[,,4,thr])

dat <- data.frame(year = s.idx$year,
                  sIdx = s.idx$meanCPUE,
                  n5Idx = n5.idx$meanCPUE)
dat$log.sIdx <- log(dat$sIdx)
dat$log.n5Idx <- log(dat$n5Idx)


# Model 1
## fit with zero-intercept, calculate rSq, pvalue, coefs, slope                      
m1 <- lm(sIdx ~ 0 + n5Idx, data=dat)
rSq1  <-summary(m1)$r.squared
pVal1 <-anova(m1)$'Pr(>F)'[1]
slope1 <- as.numeric(coef(m1))
# autoplot(m1)

newX <- seq(0.01,8,0.01)
preds1 <- slope1*newX


# Model 2
## fit with zero-intercept, calculate rSq, pvalue, coefs, slope                      
m2 <- lm(log.sIdx ~ 0+ log.n5Idx, data=dat)
rSq2  <-summary(m2)$r.squared
pVal2 <-anova(m2)$'Pr(>F)'[1]
slope2 <- as.numeric(coef(m2))
# autoplot(m2)

# add predicted fit for m2
# preds2 <- slope2*log(newX)
# preds2 <- exp(preds2)
preds2 <- predict(m2, newdata=data.frame(log.n5Idx=log(newX)))
preds2 <- exp(preds2)


# Model 3
## fit without zero-intercept, calculate rSq, pvalue, coefs, slope                      
m3 <- lm(log.sIdx ~ log.n5Idx, data=dat)
rSq3  <-summary(m3)$r.squared
pVal3 <-anova(m3)$'Pr(>F)'[1]
slope3 <- as.numeric(coef(m3))[2]
int3 <- as.numeric(coef(m3))[1]
# autoplot(m3)

# preds3 <- int3 + slope3*log(newX)
# preds3 <- exp(preds3)
preds3 <- predict(m3, newdata=data.frame(log.n5Idx=log(newX)))
preds3 <- exp(preds3)

# ---- Plot fits for all 3 models ---
max <- max(dat$n5Idx)


png(filename='Outputs/plots/lmfit_Sidx_VSn_5Aidx_m123.png',
    width=800, height=600, res=100)

par(mgp=c(3,0.8,0), tck=-.02, mar=c(4,4,0.2,0.2))
plot(y=s.idx$meanCPUE, x=n5.idx$meanCPUE, pch=20,
     xlim=c(0,max), ylim=c(0,max), las=1,
     ylab='IPHC South Index (YE Pcs/skate)',
     xlab='IPHC Northern 5A Index (YE Pcs/skate)')

text(y=s.idx$meanCPUE+0.1, x=n5.idx$meanCPUE,
     labels=s.idx$year, cex=0.5)

# add fit for linear regression
lines(x=newX, y=preds1, col='red')

# add fit for linear regression on log-scaled
lines(x=newX, y=preds2, col='purple')

# add fit for linear regression on log-scaled without 0-int
lines(x=newX, y=preds3, col='green')

abline(v=dat$n5Idx[c(1,3)], col='grey50', lty=2)

legend('topleft', lty=c(1,1,1), bg='white', cex=0.8,
      legend=c(paste('m1: sIdx ~ B1 x n5AIdx, rSq=',
                    round(rSq1,2),'p-value <0.001'),
              paste('m2: log(sIdx) ~ B1 x log(n5AIdx), rSq=', 
                    round(rSq2,2),'p-values <0.001'),
              paste('m3: log(sIdx) ~ B0 +B1 x log(n5AIdx), rSq=', round(rSq3,2),'p-values <0.001')),
      col=c('red','purple','green'))

dev.off()

# ---- Plot fits for all m3 ---

png(filename='Outputs/plots/lmfit_Sidx_VSn_5Aidx_m3.png',
    width=800, height=600, res=100)

par(mgp=c(3,0.8,0), tck=-.02, mar=c(4,4,0.2,0.2))
plot(y=s.idx$meanCPUE, x=n5.idx$meanCPUE, pch=20,
     xlim=c(0,max), ylim=c(0,max), las=1,
     ylab='IPHC South Index (YE Pcs/skate)',
     xlab='IPHC Northern 5A Index (YE Pcs/skate)')

text(y=s.idx$meanCPUE+0.1, x=n5.idx$meanCPUE,
     labels=s.idx$year, cex=0.5)

# add fit for linear regression on log-scaled without 0-int
lines(x=newX, y=preds3, col='#1b9e77', lty=1)

# Add fit for 1998 and 2001 predictions
m3Pts <- predict(m3, newdata=data.frame(log.n5Idx=log(dat$n5Idx[c(1,3)])),
                 se.fit=T)
points(x=dat$n5Idx[c(1,3)], y=exp(m3Pts$fit), col='black', bg='#1b9e77',
      pch=24)
text(x=dat$n5Idx[c(1,3)], y=exp(m3Pts$fit)+0.15,
     labels=dat$year[c(1,3)], cex=0.5, col='#1b9e77')

legend('bottom', bty='n',
      legend=paste('rSq=', round(rSq3,2),', p-value <0.001'))

dev.off()
# ----



# Calculate southern indices
dat$pred1.sIdx <- dat$sIdx
dat$pred2.sIdx <- dat$sIdx
dat$pred3.sIdx <- dat$sIdx

x <- dat$n5Idx[c(1,3)]
dat$pred1.sIdx[c(1,3)] <- slope1*x
dat$pred2.sIdx[c(1,3)] <- exp(slope2*log(x))
dat$pred3.sIdx[c(1,3)] <- exp(int3 + slope3*log(x))

# --- plot predicted indices for southern IPHC ---
png(filename='Outputs/plots/predIdx_South_m123.png',
    width=700, height=700, res=100)

par(mfrow=c(2,1), mgp=c(1.8,0.5,0), tck=-.02, 
    mar=c(3,3,0.2,0.2))

# Plot 1
# Add n5idx and +/- 1.96 SE for s.idx
plot(x=dat$year, y=dat$n5Idx, las=1, type='l',
     ylab='Mean CPUE (YE Pcs/100 hooks)', col='blue',
     xlab='year', ylim=c(0,9))
points(x=dat$year, y=dat$n5Idx, pch=16, col='blue')

segments(x0=dat$year, x1=dat$year, col='blue',
        y0=n5.idx$meanCPUE - 1.96*n5.idx$seCPUE,
        y1=n5.idx$meanCPUE + 1.96*n5.idx$seCPUE)

# Plot 2
plot(x=dat$year, y=dat$sIdx, las=1, type='l',
     ylab='Mean CPUE (YE Pcs/100 hooks)',
     xlab='year', ylim=c(0,9))
points(x=dat$year, y=dat$sIdx, pch=16)

# Add +/- 1.96 SE for s.idx
segments(x0=dat$year, x1=dat$year,
        y0=s.idx$meanCPUE - 1.96*s.idx$seCPUE,
        y1=s.idx$meanCPUE + 1.96*s.idx$seCPUE)


# Add pred1 points
points(x=dat$year[c(1,3)], pch=1, col='red',
      y=dat$pred1.sIdx[c(1,3)])
# Add pred2 points
points(x=dat$year[c(1,3)], pch=1, col='purple',
      y=dat$pred2.sIdx[c(1,3)])
# Add pred3 points
points(x=dat$year[c(1,3)], pch=1, col='green',
      y=dat$pred3.sIdx[c(1,3)])

legend('topright', pch=c(1,1,1), bg='white', bty='n',
      title='Model Predictions for 1999 and 2001 index',
      legend=c('m1','m2','m3'),
      col=c('red','purple','green'))

legend('topleft', lty=c(1,1), bg='white',bty='n',
      legend=c('South Index', 'n5A Index'),
      col=c('black','blue'))

dev.off()
# ----

# --- plot predicted indices for southern IPHC - m3 ---
png(filename='Outputs/plots/predIdx_South_m3.png',
    width=700, height=700, res=100)

par(mfrow=c(2,1), mgp=c(1.8,0.5,0), tck=-.02, 
    mar=c(3,3,0.2,0.2))

# Plot 1
# Add n5idx and +/- 1.96 SE for s.idx
plot(x=dat$year, y=dat$n5Idx, las=1, type='l',
     ylab='Mean CPUE (YE pcs/skate)', col='#d95f02',
     xlab='', ylim=c(0,11))
points(x=dat$year, y=dat$n5Idx, pch=16, col='#d95f02')

segments(x0=dat$year, x1=dat$year, col='#d95f02',
        y0=n5.idx$meanCPUE - 1.96*n5.idx$seCPUE,
        y1=n5.idx$meanCPUE + 1.96*n5.idx$seCPUE)

# Plot 2
plot(x=dat$year, y=dat$sIdx, las=1, type='l',
     ylab='Mean CPUE (YE pcs/skate)',
     xlab='', ylim=c(0,9))
points(x=dat$year, y=dat$sIdx, pch=16)

# Add +/- 1.96 SE for s.idx
segments(x0=dat$year, x1=dat$year, col='black',
        y0=s.idx$meanCPUE - 1.96*s.idx$seCPUE,
        y1=s.idx$meanCPUE + 1.96*s.idx$seCPUE)


# Add pred3 points
points(x=dat$year[c(1,3)], pch=24, col='black',bg='#1b9e77',
      y=dat$pred3.sIdx[c(1,3)])

# Add +/- 1.96 SE for s.idx
segments(x0=dat$year[c(1,3)], x1=dat$year[c(1,3)],
         y0=exp(m3Pts$fit - 1.96*m3Pts$se.fit),
         y1=exp(m3Pts$fit + 1.96*m3Pts$se.fit),
         col='#1b9e77')

legend('topright', pch=24, bg='white', bty='n',
      legend='model predicted indices for 1998 and 2001',
      pt.bg='#1b9e77',col='black')

legend('topleft', lty=c(1,1), bg='white',bty='n',
      legend=c('IPHC South Index', 'IPHC n5A Index'),
      col=c('black','#d95f02'),
      pch=16)

dev.off()
# ----



filename <- paste('tables/iphcPredIdx_South_thresh',
                  thr,'.csv',sep='')
write.csv(dat,filename, row.names=F)




