---  
geometry: letterpaper
nocaption: false
numbersections: false
header-includes:
- \usepackage{booktabs}
- \usepackage{longtable}
- \usepackage{array}
- \usepackage{multirow}
- \usepackage[table]{xcolor}
- \usepackage{wrapfig}
- \usepackage{float}
- \usepackage{colortbl}
- \usepackage{pdflscape}
- \usepackage{tabu}
- \usepackage{threeparttable}
- \usepackage{threeparttablex}
- \usepackage[normalem]{ulem}
- \usepackage{makecell}
- \usepackage{caption}
- \usepackage{pdflscape}
- \newcommand{\blandscape}{\begin{landscape}}
- \newcommand{\elandscape}{\end{landscape}}
- \usepackage{fontspec}
- \setmainfont{Calibri}
always_allow_html: yes
firstpage: 12
output: 
  pdf_document:
    keep_tex: true
    fig_caption: true
    latex_engine: xelatex
    template: ~/Documents/LANDMARK/PHMA/2018_YEassessment/yeopmod/Doc/lfrTandF.latex
  html_document:
    self_contained: true
    df_print: kable
    keep_md: false
    toc: true
  word_document:
    df_print: kable
    reference_docx: ~/Documents/LANDMARK/PHMA/2018_YEassessment/yeopmod/Doc/basic.docx
  bookdown::pdf_document2:
    keep_tex: true
    fig_caption: true
    latex_engine: xelatex
    template: ~/Documents/LANDMARK/PHMA/2018_YEassessment/yeopmod/Doc/lfrTandF.latex
    fig_width: 7
    fig_height: 6
  bookdown::html_document2:
    self_contained: true
    df_print: kable
    keep_md: false
    number_sections: false
    toc: true
  bookdown::word_document2:
    df_print: kable
    reference_docx: ~/Documents/LANDMARK/PHMA/2018_YEassessment/yeopmod/Doc/basic.docx
---

```{r SetupTandF, message=FALSE, warning=FALSE, include=FALSE, messages = FALSE}
knitr::opts_chunk$set(fig.pos = 'p') # Places figures on their own pages
knitr::opts_chunk$set(out.width = '100%', dpi=300)

# I usually load my libraries up front to keep things organized
library(bookdown)
library(knitr)
library(kableExtra)
library(stringr)
library(tidyverse)
library(PBSmapping)
library(sp)
library(maptools)
library(broom)
library(rgeos)
# library(ggsidekick)
library(raster)
library(ggmap)
library(RColorBrewer)
library(wesanderson)
library(reshape2)
library(viridis)
library(SDMTools)

options(knitr.kable.NA = '-')

source("../assessCAplots.R")
source("../reportFuns.R")
source("../mseRtools.r")

# load blobs
fitFolders <- list.files(path=here('Outputs/fits/'),
                        pattern='fit_bat')
nums <- gsub("fit_bat",fitFolders, replacement='')  %>%
        as.integer()
df <- data.frame(folder=fitFolders, fitNum = nums)

fitList <- list()
for (i in 1:length(nums))
{
  bat <- .loadFit(df$folder[df$fitNum==i])
  fitList$bat <- bat
  names(fitList)[i] <- bat$ctlList$ctrl$dataScenarioName
  
}  


stockTitles <- c( North = "North",
                  South = "South")

```

```{r, captions, include = FALSE, echo = FALSE}
depFitsMCap <- "Spawning biomass depletion for different M scenarios"

depFitsCtCap <- "Spawning biomass for different catch scenarios for reference case M=0.0345"

depFitsInitYrCap <- "Spawning biomass depletion for different start years"

```

# Tables

```{r makeTables, echo = FALSE, include = FALSE, messages = FALSE }
# Here we need to draw in the multiple report objects 
# and create tables of their reference points and 
# observation model CVs

fitCVTable <- lapply(X = fitList, FUN = getCVs  ) 
fitCVTable <- do.call( rbind, fitCVTable )
rownames(fitCVTable) <- NULL

# Now get parameter estimates
fitParTable <- lapply(X = fitList, FUN = getEstPars  ) 
fitParTable <- do.call( rbind, fitParTable )
rownames(fitParTable) <- NULL



# Need to overwrite some of the table column names
newColNamesCV <- c(BatchNum ='OM #',
                  Scenario = "Data Scenario",
                  # Hypothesis = "Stock Structure Hypothesis",
                  Stock = "Stock",
                  PHMA_Nidx = "PHMA_N Index",
                  PHMA_QCSidx = "PHMA_QCS Index",
                  PHMA_Sidx = "PHMA_S Index",
                  IPHC_Nidx  = "IPHC_N Index",
                  IPHC_Sidx  = "IPHC_S Index",
                  PHMA_Nage = "PHMA_N Ages",
                  PHMA_QCSage = "PHMA_QCS Ages",
                  PHMA_Sage = "PHMA_S Ages",
                  IPHC_Nage = "IPHC_N Ages",
                  IPHC_Sage = "IPHC_S Ages",
                  LL_age = "Longline Ages",
                  LL_CWage = "Longline Ages Mixed",
                  TRAWL_Nage = "Trawl Ages")


newColNamesPar <- c(  BatchNum ='OM #',
                      Scenario = "Data Scenario",
                      # Hypothesis = "Stock Structure Hypothesis",
                      Stock = "Stock",
                      B0 = "$B_0$",
                      R0 = "$R_0$",
                      meanRecDev = "$\\\\omega_{R}$",
                      rSteep = "h",
                      M = "$M_0$",
                      Bmsy = "$B_{MSY}$",
                      Fmsy = "$F_{MSY}$",
                      # Umsy = "$U_{MSY}$",
                      MSY = "$MSY$",
                      BT = "$B_T$",
                      "BT/B0" = "$B_T/B_0$",
                      "BT/Bmsy" = "$B_T/B_{MSY}$" )

informTitlesPar <- c(    " "= 1,
                         " "= 1,
                         " "= 1,
                         "Unfished\nBiomass (kt)"= 1,
                         "Unfished\nRecruitment (1e6)"= 1,
                         "Mean recruitment\nlog-residuals"=1,
                         "Stock-recruit\nSteepness"= 1,
                         "Natural\nMortality"= 1,
                         "Optimal\nbiomass (kt)"= 1,
                         "Optimal\nfishing mortality"= 1,
                         "Maximum Sustainable\nYield (kt)"= 1,
                         "Current\nBiomass (kt)"= 1,
                         "Biomass\nDepletion"= 2 )


```



```{r printFitCVTable, echo = FALSE, messages = FALSE }

capText <- "Estimated observation model standard errors under different data/model scenarios."

#

fitCVTable <- dplyr::select(fitCVTable, -Hypothesis)
                
colnames(fitCVTable) <- newColNamesCV

kable(  fitCVTable, escape = FALSE,
        booktabs = T,
        caption = capText,
        align = rep("c",ncol(fitCVTable))) %>%
  kable_styling(latex_options = c("striped", "hold_position","scale_down"),
                bootstrap_options = c("striped", "hover")) %>%
  add_header_above( c(" " = 3, 
                    "Biomass index observation errors" = 5, 
                    "Age composition observation errors" = 8), bold = T )
  

```

\newpage

\blandscape

```{r printFitParTable, echo = FALSE, messages = FALSE }

capText <- "Biological reference points and management parameter estimates under different data/model scenarios."

fitParTable <- dplyr::select(fitParTable, -Hypothesis)

colnames(fitParTable) <- newColNamesPar


kable(  fitParTable, escape = FALSE,
        booktabs = T,
        caption = capText,
        align = rep("c",ncol(fitParTable))) %>%
  kable_styling(latex_options = c("striped", "hold_position","scale_down"),
                bootstrap_options = c("striped", "hover")) %>%
  add_header_above(informTitlesPar)


```

# Figures

```{r, compareBt1, echo = FALSE, message = FALSE, fig.cap = depFitsMCap }

    # #Specify M scenarios for plotting
    # namesM <- c("baseM_1960_str2018",
    #             "M.03_1960_str2018",
    #             "M.04_1960_str2018",
    #             "M.05_1960_str2018",
    #             "M.06_1960_str2018")
    # fitsM <- fitList[names(fitList) %in% namesM]

    # compareBt(fitObjList=fitsM)
```

```{r, compareBt2, echo = FALSE, message = FALSE, fig.cap = depFitsCtCap }

    # # Specify Catch Scenarios for plotting
    # namesCt <-      c("baseM_1960_str2018",
    #                   "baseM_1960_gmPHMAgamma2018",
    #                   "baseM_1960_lbCommCatch")
    # fitsCt <- fitList[names(fitList) %in% namesCt]
  
    # compareBt(fitObjList=fitsCt)
```

```{r, compareBt3, echo = FALSE, message = FALSE, fig.cap = depFitsInitYrCap }

    # # Specify scenarios for init year
    # namesInitYr <- c( "M.03_1960_str2018",       
    #                   "M.04_1960_str2018",        
    #                   "M.05_1960_str2018",         
    #                   "M.06_1960_str2018",
    #                   "M.03_1918_str2018",
    #                   "M.04_1918_str2018",        
    #                   "M.05_1918_str2018",        
    #                   "M.06_1918_str2018")

    # fitsInitYr <- fitList[names(fitList)  %in% namesInitYr]

    # clrs <- brewer.pal(length(fitsInitYr)/2, 'Set1')
    # clrs <- c(clrs,clrs)
    # lines <- c(rep(1,length(fitsInitYr)/2),
    #            rep(2,length(fitsInitYr)/2))


    # compareBt(fitObjList=fitsInitYr, clrs=clrs, lines=lines)
```


