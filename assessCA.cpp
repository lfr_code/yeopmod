//********************************************************************
// Module: assessCA.cpp
// Authors: S.P. Cox, A.R. Kronlund, K.R. Holt, S. D. N. Johnson, B Doherty
// Procedure: Statistical catch-age model
//
// References: 
//
// Revised from single fleet to multifleet by S. D. N. Johnson
// Date Last Revised: June 18, 2019
// 
// Quick multifleet statistical catch at age model. Uses
// Popes approximation to approximate F for each fleet, with 
// a given fleet timing. Assumes no uncertainty in growth 
// model (all fish lie perfectly along vonB growth curve). 
// Modified from ADMB single fleet model assessCA.tpl
// 
// Features:
//  1. Pope's approx of F_tgp values
//  2. Logistic normal age obs likelihood with cond MLE variance
// 
// ToDo:
//  4. Check beta prior moment matching scaling
//  5. Update SimulFleets code.
// 
//*******************************************************************/

#include <TMB.hpp>       // Links in the TMB libraries
#include <iostream>

// posfun
template<class Type>
Type posfun(Type x, Type eps, Type &pen)
{
  pen += CppAD::CondExpLt(x, eps, Type(0.01) * pow(x-eps,2), Type(0));
  return CppAD::CondExpGe(x, eps, x, eps/(Type(2)-x/eps));
}

// invLogit
template<class Type>
Type invLogit(Type x, Type scale, Type trans)
{
  return scale/(Type(1.0) + exp(-Type(1.0)*x)) - trans;
}

// dinvgamma()
// R-style inverse gamma probability distribution density
// function.
// inputs:    x = real number to calculate density of
//            alpha = shape parameter
//            beta = scale parameter
//            logscale = int determining whether function returns log (1) or 
//                        natural (0) scale density
// outputs:   dens = density on natural or log scale
// Usage:     usually for variance priors
// Source:    S. D. N. Johnson
template<class Type>
Type dinvgamma( Type x,
                Type alpha,
                Type beta,
                int logscale )
{

  Type dens = -1 * ( (alpha + 1) * log(x) + beta / x );

  if( logscale == 0 )
    dens = exp( dens );

  return(dens);
}


// calcIGbeta()
// Calculates IG beta parameter from
// a prior mode and alpha parameter
// inputs:    alpha = shape parameter
//            mode  = prior mode parameter
// outputs:   beta  = beta parameter solved from prior mode
// Usage:     usually for variance priors
// Source:    S. D. N. Johnson
template<class Type>
vector<Type> calcIGbeta(  vector<Type> mode,
                          vector<Type> alpha )
{
  // Get length of mode vector
  int lenVec = mode.size();

  // Create beta vector
  vector<Type> beta(lenVec);

  beta = mode * (alpha + 1);

  return(beta);
}


// invLogit
template<class Type>
Type square(Type x)
{
  return x*x;
}

// calcLogistNormLikelihood()
// Calculates the logistic normal likelihood for compositional data.
// Automatically accumulates proportions below a given threshold. Takes
// cumulative sum of squared resids and number of classes for which
// residuals are calculated as pointers, so that these can be accumulated
// across years for conditional MLE of variance. 
// Will extend to correlated residuals and time-varying variance later.
// inputs:    yObs    = Type vector of observed compositions (samples or proportions)
//            pPred   = Type vector of predicted parameters (true class proportions)
//            minProp = minimum proportion threshold to accumulate classes above
//            etaSumSq= cumulative sum of squared 0-mean resids
//            nResids = cumulative sum of composition classes (post-accumulation)
// outputs:   resids = vector of resids (accumulated to match bins >= minProp)
// Usage:     For computing the likelihood of observed compositional data
// Source:    S. D. N. Johnson
// Reference: Schnute and Haigh, 2007; Francis, 2014
template<class Type>
vector<Type> calcLogistNormLikelihood_tc( vector<Type>& yObs, 
                                          vector<Type>& pPred,
                                          Type minProp,
                                          Type& etaSumSq,
                                          Type& nResids )

{
  // Get size of vector 
  int nX = yObs.size();

  // Normalise the samples and predictions in case they are numbers
  // and not proportions
  yObs /= yObs.sum();
  pPred /= pPred.sum();

  // Create vector of residuals to return
  vector<Type> resids(nX);
  vector<Type> aboveInd(nX);
  resids.setZero();
  aboveInd.setZero();

  // Count number of observations less
  // than minProp
  int nAbove = 0;
  for( int x = 0; x < nX; x++)
    if(yObs(x) > minProp)
    {
      nAbove++;
      aboveInd(x) = 1;
    }

  // Now loop and fill
  vector<Type> newObs(nAbove);
  vector<Type> newPred(nAbove);
  newObs.setZero();
  newPred.setZero();
  int k = 0;
  for( int x = 0; x < nX; x++)
  {
    // accumulate observations
    newObs(k) += yObs(x);
    newPred(k) += pPred(x);

    // Increment k if we reach a bin
    // with higher than minProp observations
    // and we aren't yet in the last bin
    if(yObs(x) >= minProp & k < nAbove - 1)
      k++;    
  }

  // Create a residual vector
  vector<Type> res(nAbove);
  res.setZero(); 
  
  for( int k = 0; k < nAbove; k++)
    if( newObs(k) > 0  &  newPred(k) > 0)
      res(k) = log(newObs(k)) - log(newPred(k));

  // Calculate mean residual
  Type meanRes = res.sum()/nAbove;
  // centre residuals
  res -= meanRes;


  // Now loop again, and fill in
  // the residuals vector
  // for plotting later
  k = 0;
  for( int x =0; x < nX; x++)
    if( aboveInd(x) == 1)
    {
      resids(x) = res(k);
      k++;
    }

  
  // Now add squared resids to etaSumSq and nRes to nResids
  etaSumSq  += (res*res).sum();
  nResids   += nAbove;

  return(resids);
} // end calcLogistNormLikelihood_tc()


// calcLogistNormLikelihood()
// Calculates the logistic normal likelihood for compositional data.
// Automatically accumulates proportions below a given threshold. Takes
// cumulative sum of squared resids and number of classes for which
// residuals are calculated as pointers, so that these can be accumulated
// across years for conditional MLE of variance. 
// Will extend to correlated residuals and time-varying variance later.
// inputs:    yObs    = Type vector of observed compositions (samples or proportions)
//            pPred   = Type vector of predicted parameters (true class proportions)
//            minProp = minimum proportion threshold to accumulate classes above
//            etaSumSq= cumulative sum of squared 0-mean resids
//            nResids = cumulative sum of composition classes (post-accumulation)
// outputs:   NA, void function, updates etaSumSq and nResids
// Usage:     For computing the likelihood of observed compositional data
// Source:    S. D. N. Johnson
// Reference: Schnute and Haigh, 2007; Francis, 2014
template<class Type>
void calcLogistNormLikelihood(  vector<Type> yObs, 
                                vector<Type> pPred,
                                Type minProp,
                                int minX,
                                Type& etaSumSq,
                                Type& nResids )

{
  // Get size of vector 
  int nX = yObs.size();

  // Normalise the observed samples in case they are numbers
  // and not proportions
  yObs /= yObs.sum();

  // Create an indicator vector for elements
  // less than minProp, use 1 extra class for
  // potential accumulation
  vector<Type> belowInd(nX);
  vector<Type> aboveInd(nX);
  belowInd.setZero();
  aboveInd.fill(1.0);
  // Create accumulator classes
  Type accumObs = 0.;
  Type accumPred = 0.;

  // Create a residual vector
  vector<Type> res(nX);
  res.setZero(); 
  // Create a container for accumulation class resids
  Type accumRes = 0.;
  
  // Now loop over compositions, check for
  // proportions < minProp
  for(int x = minX - 1; x < nX; x++ )
  {
    if(yObs(x) < minProp)
      belowInd(x) = 1.;
    else
      res(x) += log( yObs(x) ) - log( pPred(x) );
  }
  
  // Adjust aboveInd by subtracting belowInd
  aboveInd -= belowInd;

  // Caclulate accummulator class proportions
  accumObs  += (belowInd * yObs).sum();
  accumPred += (belowInd * pPred).sum();

  // Count residuals
  Type nRes = aboveInd.sum() - minX + 1;
  
  if( accumObs > 0 )
  {
    accumRes += log( accumObs ) - log( accumPred );
    nRes += 1;
  }

  // Calculate mean residual
  Type meanRes = res.sum()/nRes;
  // centre residuals
  res -= aboveInd * meanRes;
  if( accumObs > 0)
    accumRes -= meanRes;

  // Now add squared resids to etaSumSq and nRes to nResids
  etaSumSq += (res*res).sum() + pow(accumRes,2);
  nResids += nRes;

} // end calcLogistNormLikelihood()



// objective function
template<class Type>
Type objective_function<Type>::operator() ()
{
  // Call namespaces //
  using namespace density;

  /*\/\/\/\/\ DATA SECTION /\/\/\/\*/
  // Data Structures
  DATA_ARRAY(I_tgp);            // CPUE data by time, gear, stock (-1 == missing)
  DATA_ARRAY(C_tgp);            // Catch data by time, gear, stock
  DATA_ARRAY(A_atgp);           // Age observations by age, time, gear, stock ( -1 == missing )
  DATA_ARRAY(mA_atg);           // Age observations by age, time, gear, for mixed stocks

  // Model dimensions
  int nG = I_tgp.dim(1);        // No. of surveys
  int nT = I_tgp.dim(0);        // No of time steps
  int nP = I_tgp.dim(2);        // No. of stocks
  int nA = A_atgp.dim(0);       // No of age classes

  // Model switches
  DATA_IVECTOR(survType_g);     // Type of index (0 = vuln bio, 1 = vuln numbers)
  DATA_IVECTOR(indexType_g);    // Type of survey (0 = relative, 1 = absolute)
  DATA_IVECTOR(calcIndex_g);    // Calculate fleet index (0 = no, yes = 1)
  DATA_IVECTOR(mixedAgeComp_g); // Used coastwide (i.e. 'mixed') age comps (0 = no, yes = 1)
  DATA_VECTOR(ageCompWeight_g); // Age composition likelihood weight scalar
  DATA_IVECTOR(selType_g);      // Type of selectivity (0 = asymptotic, 1 = domed (normal))
  DATA_IVECTOR(selLen_g);       // Len or Age base selectivity (0 = age, 1 = length)
  DATA_IVECTOR(tvSelFleets);    // Switch for time-varying selectivity or not
  DATA_VECTOR(fleetTiming);     // Fraction of year before catch/survey observation
  DATA_INTEGER(initCode);       // initialise at 0 => unfished, 1=> fished
  DATA_INTEGER(useMsen);        // separate M for +group, 0 => no Msen, 1=> use Msen
  DATA_INTEGER(selPriorOn);     // Use prior on selectivity (0=> no prior, 1=> use prior)
  DATA_SCALAR(posPenFactor);    // Positive-penalty multiplication factor
  DATA_INTEGER(firstRecDev);    // First recruitment deviation
  DATA_INTEGER(lastRecDev);     // First recruitment deviation
  DATA_SCALAR(minPropAge);      // Minimum proportion in age comps (< minPropage are accumulated)
  DATA_INTEGER(minAge);         // minimum age to be considered in age comp data
  DATA_INTEGER(minMatAge);      // minimum age for maturity ogive prob>0
  DATA_MATRIX(Q);               // ageing error matrix


  /*\/\/\/\/\ PARAMETER SECTION /\/\/\/\*/
  // Leading biological parameters //
  PARAMETER_VECTOR(lnB0_p);             // Unfished spawning biomass for each stock
  PARAMETER(logit_ySteepness);          // SR steepness on (0,1)
  PARAMETER(lnM);                       // Natural mortality rates
  PARAMETER(lnMsen);                    // Senescence mortality rate on plus group
  PARAMETER_ARRAY(log_initN_ap);        // Non-eq initial numbers multiplier (nAxnP array)

  // Random stock effects on biological pars
  PARAMETER_VECTOR(epsM_p);             // Stock specific M deviation
  PARAMETER(lnsigmaMp);                 // Stock M effect log-SD
  PARAMETER_VECTOR(epsSteep_p);         // Stock specific steepness deviation on logit scale
  PARAMETER(lnsigmaSteepp);             // Stock steepness effect log-SD

  // Observation models //
  // Selectivity
  PARAMETER_VECTOR(lnSelAlpha_g);       // log scale selectivity alpha par (ageSel50, lenSel50, or mu in N model)
  PARAMETER_VECTOR(lnSelBeta_g);        // log scale selectivity beta par (age/len sel step, or sigma in N model)
  PARAMETER_VECTOR(epsSelAlpha_vec);    // log-normal error in x-at-50% sel
  PARAMETER_VECTOR(epsSelBeta_vec);     // log-normal error in x-at-95% sel
  PARAMETER_VECTOR(lnsigmaSelAlpha_g);  // log-SD of SelAlpha deviations
  PARAMETER_VECTOR(lnsigmaSelBeta_g);   // log-SD of SelBeta deviations

  // Survey obs error and catchabilities are
  // concentrated as cond. MLEs

  // Process errors //
  PARAMETER_VECTOR(recDevs_vec);        // Recruitment log-deviations
  PARAMETER(lnsigmaR);                  // log scale recruitment SD          
  PARAMETER_ARRAY(omegaM_pt);           // Natural mortality log-deviations
  PARAMETER(lnsigmaM);                  // log scale natural mortality SD

  // Priors //
  PARAMETER_VECTOR(obstau2IGa);         // Inverse Gamma Prior alpha for stock index obs error var prior
  PARAMETER_VECTOR(obstau2IGmode);      // Inverse Gamma Prior mode for stock index obs error var prior
  PARAMETER_VECTOR(sig2RPrior);         // Hyperparameters for sig2R prior - (IGa,IGb) or (mean,var)
  PARAMETER_VECTOR(sig2MPrior);         // Hyperparameters for sig2M prior - (IGa,IGb) or (mean,var)
  PARAMETER_VECTOR(rSteepBetaPrior);    // pars for Beta dist steepness prior
  PARAMETER_VECTOR(initMPrior);         // pars for initial M prior (mean,sd)
  PARAMETER_VECTOR(mq);                 // prior mean catchability 
  PARAMETER_VECTOR(sdq);                // prior sd on mean catchability 

  // Fixed LH parameters
  PARAMETER_VECTOR( aMat );             // Maturity ogive parameters (ages at 50% and 95% mature)
  PARAMETER( Linf );                    // Asymptotic length
  PARAMETER( L1 );                      // Length at age 1
  PARAMETER( vonK );                    // vonB growth rate
  PARAMETER_VECTOR( lenWt );            // Allometric length/weight c1, c2 parameters

  // Max selectivity age
  PARAMETER_VECTOR(mSelMode_g);         // prior mean selectivity mode
  PARAMETER(selModeCV);                 // selectivity mode/a95 CV
  
  

  // Derived Variables //
  // Transformed scalar parameters
  vector<Type>  B0_p          = exp(lnB0_p);
  Type          M             = exp(lnM);
  vector<Type>  M_p           = exp(lnM + epsM_p );
  Type          Msen          = exp(lnMsen);
  Type          ySteepness    = invLogit(logit_ySteepness,Type(1), Type(0.));
  Type          rSteepness    = 0.2 + 0.8 * ySteepness;
  Type          sigmaR        = exp(lnsigmaR);
  Type          sigmaM        = exp(lnsigmaM);
  Type          sigmaMp       = exp(lnsigmaMp);
  Type          sigmaSteepp   = exp(lnsigmaSteepp);

  // Make stock-specific steepness pars
  vector<Type>  ySteepness_p(nP);
  for( int p = 0; p < nP; p++)
    ySteepness_p(p) = invLogit(logit_ySteepness + epsSteep_p(p), Type(1), Type(0));
  vector<Type>  rSteepness_p  = 0.2 + 0.8 * ySteepness_p;

  // Transformed Selectivity model parameter vectors
  vector<Type> SelAlpha_g       = exp(lnSelAlpha_g);
  vector<Type> SelBeta_g        = exp(lnSelBeta_g);
  vector<Type> sigmaSelAlpha_g  = exp(lnsigmaSelAlpha_g);
  vector<Type> sigmaSelBeta_g   = exp(lnsigmaSelBeta_g);

  // Transformed parameters in arrays require loops to fill them
  // Initial N multipliers
  array<Type> initN_mult_ap(nA,nP);
  // Time-varying selectivity
  array<Type> SelAlpha_gtp(nG,nT,nP);
  array<Type> SelBeta_gtp(nG,nT,nP);
  // Loop and fill
  for( int p = 0; p < nP; p++)
  {
    initN_mult_ap.col(p) = exp(log_initN_ap.col(p));
    SelAlpha_gtp.col(p).col(0) = SelAlpha_g;
    SelBeta_gtp.col(p).col(0) = SelBeta_g;
  }

  // Loop over time steps and gears, create a matrix
  // of deviations in selectivity
  array<Type> epsSelAlpha_gtp(nG,nT,nP);
  array<Type> epsSelBeta_gtp(nG,nT,nP);
  int epsSelVecIdx = 0;
  for( int p = 0; p < nP; p++ )
    for( int t = 0; t < nT; t++ )
    { 
      // Bring previous time-step selecivity
      // forward
      if( t > 0 )
      {
        SelAlpha_gtp.col(p).col(t) = SelAlpha_gtp.col(p).col(t-1);
        SelBeta_gtp.col(p).col(t) = SelBeta_gtp.col(p).col(t-1);
      }
      // Update the epsilon array
      for( int g = 0; g < nG; g++ )
        if( (tvSelFleets(g) == 1) & (A_atgp(0,t,g,p) >= 0) )
        {
          epsSelAlpha_gtp(g,t,p) += epsSelAlpha_vec(epsSelVecIdx);
          epsSelBeta_gtp(g,t,p) += epsSelBeta_vec(epsSelVecIdx);
          epsSelVecIdx++;
        }    
      // Now update SelAlpha_gt and SelBeta_gt - can happen
      // at first time step, since we're using the gear specific
      // selectivity curve when we don't age observations
      SelAlpha_gtp.col(p).col(t) *= exp(epsSelAlpha_gtp.col(p).col(t));
      SelBeta_gtp.col(p).col(t) *= exp(epsSelBeta_gtp.col(p).col(t));
      
    }


  // Stock recruit model
  vector<Type> reca_p(nP);             // BH a par
  vector<Type> recb_p(nP);             // BH b par
  vector<Type> phi_p(nP);              // ssbpr
  vector<Type> R0_p(nP);               // unfished eqbm recruitment
  reca_p.setZero();
  recb_p.setZero();
  phi_p.setZero();
  R0_p.setZero();

  // Recruitment devs
  array<Type> omegaR_tp(nT,nP);
  omegaR_tp.setZero();
  int recVecIdx = 0;
  for( int p = 0; p < nP; p++ )
    for(int t = firstRecDev-1; t < lastRecDev; t++)
    {
      omegaR_tp(t,p) = recDevs_vec(recVecIdx);
      recVecIdx++;
    }

  // Optimisation and modeling quantities
  Type objFun = 0.;                   // Objective function
  Type posPen = 0.;                   // posFun penalty
  Type recNLP = 0.;                   // recruitment deviations NLL
  Type mortNLP = 0.;                  // Mt devations NLL
  Type qNLP = 0.0;                    // q neg log prior density
  array<Type>  selNLP_gp(nG,nP);      // Selectivity NLP
  array<Type>  obsIdxNLL_gp(nG,nP);   // observation indices NLL
  array<Type>  ageObsNLL_gp(nG,nP);   // Age observation NLL
  array<Type>  tau2Age_gp(nG,nP);     // Age observation variance
  array<Type>  etaSumSq_gp(nG,nP);    // Sum of squared age logist resids
  array<Type>  nResids_gp(nG,nP);     // Number of residuals
  array<Type>  nObsAge_gp(nG,nP);     // Number of discrete years with observations
  array<Type>  nlptau2idx_gp(nG,nP);  // Observation error var prior

  // Initialise vectors at 0
  obsIdxNLL_gp.fill(0.0);
  ageObsNLL_gp.fill(0.0);
  tau2Age_gp.fill(0.0);
  nlptau2idx_gp.fill(0.0);
  selNLP_gp.fill(0.0);

  // Life history schedules
  vector<Type>  age(nA);
  vector<Type>  wt(nA);
  vector<Type>  len(nA);
  vector<Type>  mat(nA);
  array<Type>   surv_pa(nP,nA);

  // Selectivity values
  array<Type>   sel_ag(nA,nG);  
  sel_ag.setZero();
  // Time-varying selectivity
  array<Type>   sel_agtp(nA,nG,nT,nP);
  sel_agtp.setZero();
  // Get max sel age
  vector<Type>  selModeX_g(nG);
  selModeX_g.fill(0.); 


  // State variables
  array<Type>   N_atp(nA,nT+1,nP);            // Numbers at age
  array<Type>   B_atp(nA,nT+1,nP);            // Total biomass at age
  array<Type>   vulnB_atgp(nA,nT,nG,nP);      // Vulnerable biomass at age by gear
  array<Type>   vulnB_tgp(nT,nG,nP);          // Vulnerable biomass by gear
  array<Type>   vulnN_atgp(nA,nT,nG,nP);      // Vulnerable numbers at age by gear
  array<Type>   vulnN_tgp(nT,nG,nP);          // Vulnerable numbers by gear
  array<Type>   SB_pt(nP,nT+1);               // Spawning biomass
  array<Type>   R_pt(nP,nT+1);                // Recruitments
  array<Type>   M_pt(nP,nT+1);                // Natural mortality vector
  array<Type>   F_tgp(nT,nG,nP);              // Fleet fishing mortality
  array<Type>   uAge_atgp(nA,nT,nG,nP);       // Vuln proportion at age in each gear at each time
  array<Type>   catAge_atgp(nA,nT,nG,nP);     // Catch at age in each gear at each time step
  array<Type>   predPA_atgp(nA,nT,nG,nP);     // Predicted proportion at age in each gear at each time step
  array<Type>   ageResids_atgp(nA,nT,nG,nP);  // Age residuals from calcLogisticNormLikelihood_tc()
  array<Type>   ageResMix_atg(nA,nT,nG);      // Age residuals from calcLogisticNormLikelihood_tc()

  // We need to weight proportion caught at age
  // by the proportion of vulnerable fish
  // in each stock later, so we need arrays to
  // hold that info
  array<Type>   mixedVulnN_atg(nA,nT,nG);  // Proportion of vulnerable numbers in each age class

  // Management quantities
  vector<Type>  termDep_p(nP);     // Terminal depletion
  // Type          projSpawnBio;   // Projected spawning biomass
  // vector<Type>  projExpBio;     // Projected exploitable biomass

  // Observation model quantities
  array<Type>   zSum_gp(nG,nP);     // sum of log(It/Bt)
  array<int>    validObs_gp(nG,nP); // Number of observations
  array<Type>   z_tgp(nT,nG,nP);    // array to hold residuals for NLL calc
  array<Type>   SSR_gp(nG,nP);      // sum of squared resids

  // Conditional MLEs for survey observations
  array<Type>  lnqhat_gp(nG,nP);    // log scale q_g
  array<Type>  qhat_gp(nG,nP);      // natural scale
  array<Type>  lntauObs_gp(nG,nP);  // log scale obs err SD
  array<Type>  tauObs_gp(nG,nP);    // obs err SD

  // Initialise all arrays at 0
  N_atp.fill(0.0);
  B_atp.fill(0.0);
  vulnB_atgp.fill(0.0);
  vulnB_tgp.fill(0.0);
  vulnN_atgp.fill(0.0);
  vulnN_tgp.fill(0.0);
  SB_pt.fill(0.0);
  R_pt.fill(0.0);
  M_pt.fill(0.0);
  F_tgp.fill(0.0);
  uAge_atgp.fill(0.0);
  catAge_atgp.fill(0.0);
  predPA_atgp.fill(-1);
  mixedVulnN_atg.fill(0.0);
  ageResids_atgp.setZero();


  /*\/\/\/\/\ PROCEDURE SECTION /\/\/\/\*/
  // Calculate life schedules - growth, maturity
  // Fill ages vector
  for( int a = 0; a < nA; a++)
    age(a) = a+1;
  // Calculate length curve
  len = Linf + (L1-Linf)*exp(-vonK*(age-1.));
  // Weight
  wt  = lenWt(0)*pow(len,lenWt(1));
  // Maturity
  Type tmp = log(19.)/( aMat(1) - aMat(0) );
  mat = 1./( 1. + exp(-tmp*( age - aMat(0) ) ) );

  // Set maturity=0 for ages < minMatAge
  if(minMatAge >1)
    for( int a = 0; a < (minMatAge-1); a++)
        mat(a) = 0;

  // Calculate Mortality time series
  // First and last year uses the
  // average/initial M
  M_pt.col(0)   = M_p;
  M_pt.col(nT)  = M_p;
  for( int t = 1; t < nT; t++ )
    M_pt.col(t) = M_pt.col(t-1)*exp(omegaM_pt.col(t-1));

  // Calculate selectivity
  for( int g = 0; g < nG; g++ )
  {
    Type selX = 0.;
    // Check seltype switch (asymptotic vs dome)
    if( selType_g(g) == 0)
    {
      // asymptotic
      Type xSel50   = SelAlpha_g(g);
      Type xSel95   = SelAlpha_g(g) + SelBeta_g(g);
      Type tmp      = log(19.)/( xSel95 - xSel50 );
      for( int a = 0; a < nA; a++ )
      {
        if( selLen_g(g) == 1 )
          selX = len(a);
        if( selLen_g(g) == 0)
          selX = age(a);

        sel_ag(a,g) = 1./( 1. + exp(-tmp*( selX - xSel50 ) ) );
      }

      selModeX_g(g) = xSel95;
    }

    if( selType_g(g) == 1)
    {
      // domed Normal selectivity
      Type nSelMean = SelAlpha_g(g);
      Type nSelSD   = SelBeta_g(g);
      for( int a = 0; a < nA; a++)
      {
        // Check if length or age based selectivity
        if( selLen_g(g) == 1 )
          selX = len(a);
        if( selLen_g(g) == 0)
          selX = age(a);
        // Compute selectivity function
        sel_ag(a,g) = exp(-1. * square((selX - nSelMean)/nSelSD) );
      }
      // Save mode age/length for a prior later
      selModeX_g(g) = nSelMean;
    }    

    // Now do time-varying selectivity by stock
    for( int p = 0; p < nP; p++)
      for( int t = 0; t < nT; t++)
      {
        Type selX = 0.;
        // Check seltype switch (asymptotic vs dome)
        if( selType_g(g) == 0)
        {
          // asymptotic
          Type xSel50   = SelAlpha_gtp(g,t,p);
          Type xSel95   = SelAlpha_gtp(g,t,p) + SelBeta_gtp(g,t,p);
          Type tmp      = log(19.)/( xSel95 - xSel50 );
          for( int a = 0; a < nA; a++ )
          {
            if( selLen_g(g) == 1 )
              selX = len(a);
            if( selLen_g(g) == 0)
              selX = age(a);

            sel_agtp(a,g,t,p) = 1./( 1. + exp(-tmp*( selX - xSel50 ) ) );
          }
        }

        if( selType_g(g) == 1)
        {
          // domed Normal selectivity
          Type nSelMean = SelAlpha_gtp(g,t,p);
          Type nSelSD   = SelBeta_gtp(g,t,p);
          for( int a = 0; a < nA; a++)
          {
            // Check if length or age based selectivity
            if( selLen_g(g) == 1 )
              selX = len(a);
            if( selLen_g(g) == 0)
              selX = age(a);
            // Compute selectivity function
            sel_agtp(a,g,t,p) = exp(-1. * square((selX - nSelMean)/nSelSD) );
          }
        }    
      }
  }

  // Loop through all fleets in 2 nested loops, and build a 
  // new vector of indices in chronological order
  Type          minTime = 0;
  Type          prevTime = 0;
  vector<int>   usedFleet(nG);
  vector<int>   chronIdx(nG);

  usedFleet.fill(int(0));
  chronIdx.fill(int(0));

  // Initialise fleet timing M fracs
  Type fracM = 0.;
  Type lastFrac = 0.;

  for( int gg = 0; gg < nG; gg++ )
  {
    // Set max time to end of year (hiT), and
    // lowG (idx with next smallest time) as the first fleet
    Type        hiT = 1;
    int         lowG = 0;
    // Loop over fleet timing vector, pull
    // get gear idx of minimum year fraction
    for( int gIdx = 0; gIdx < nG; gIdx++ )
    {
      // if( usedFleet(gIdx) == 1 )
      //   next();

      if( ( fleetTiming(gIdx) >= minTime) & 
          ( fleetTiming(gIdx) <= hiT ) &
          ( usedFleet(gIdx) == 0) )
      {
        // Record index of new lowest time that hasn't been used
        lowG      = gIdx;
        hiT       = fleetTiming(gIdx);
      }
    }
    chronIdx(gg)    = lowG;
    usedFleet(lowG) = int(1);
    prevTime        = minTime;
    minTime         = fleetTiming(lowG);
  }


  // Calculate SSBpr, recruitment parameters
  // Calculate equilibrium unfished survivorship.
  surv_pa.col(0).fill(1.0);
  for(int a = 1; a < nA; a++ )
    surv_pa.col(a) = exp(-M_p*a);

  // Use same M for plus group
  if(useMsen == 0)
    surv_pa.col(nA-1) /= (1.0 - exp(-M_p));

  // Use senescence M for plus group
  if(useMsen == 1)
    surv_pa.col(nA-1) /= (1.0 - exp(-Msen));
  

  // SSBpr
  for( int p = 0; p < nP; p++)
    for( int a = 0; a < nA; a ++)
      phi_p(p) += ( mat(a) * wt(a) * surv_pa(p,a) );

  // Now compute R0 from B0
  R0_p = B0_p/phi_p;

  // Beverton-Holt a parameter.
  reca_p = 4.*rSteepness_p*R0_p/(B0_p*(1.-rSteepness_p));
  // Beverton-Holt b parameter.
  recb_p = (5.*rSteepness_p-1.)/(B0_p*(1.-rSteepness_p));
    
  // Initialise population
  for( int p = 0; p < nP; p++ )
  {
    for( int a = 0; a < nA; a++)
    {
      N_atp(a,0,p) = R0_p(p) * surv_pa(p,a);
      if( initCode == 1 )
        N_atp(a,0,p) *= initN_mult_ap(a,p);
    }
    
    
    // Calc biomass, spawning biomass and rec in first year
    B_atp.col(p).col(0) = N_atp.col(p).col(0) * wt;
    SB_pt(p,0) = sum(B_atp.col(p).col(0) * mat);
    R_pt(p,0)  = N_atp(0,0,p);

    // Loop over time steps, run pop dynamics
    for( int t = 1; t <= nT; t++ )
    {
      // First, get the previous year's beginning of year numbers
      vector<Type>  tmpN_at = N_atp.col(p).col(t-1);

      // Now compute loop over fleets and take
      // catch as necessary
      Type prevTime = 0.;
      for( int cIdx = 0; cIdx < nG; cIdx ++ )
      {
        // Get actual fleet number
        int gIdx = chronIdx(cIdx);
        // Get fraction of M being used to reduce Nat
        fracM = fleetTiming(gIdx) - prevTime;

        // First, vulnerable numbers is found by reducing
        // numbers by fracM
        tmpN_at = tmpN_at * exp( - fracM * M_pt(p,t-1) );
        for(int a = 0; a < nA; a++)
        {
          vulnN_atgp(a,t-1,gIdx,p) = tmpN_at(a) * sel_agtp(a,gIdx,t-1,p);
          vulnB_atgp(a,t-1,gIdx,p) = vulnN_atgp(a,t-1,gIdx,p)*wt(a);  
          vulnB_tgp(t-1,gIdx,p) += vulnB_atgp(a,t-1,gIdx,p);
          vulnN_tgp(t-1,gIdx,p) += vulnN_atgp(a,t-1,gIdx,p);
          mixedVulnN_atg(a,t-1,gIdx) += vulnN_atgp(a,t-1,gIdx,p);
        }

        for(int a = 0; a < nA; a ++ )
        {
          // Caclulate proportion of each age in each fleet's 
          // vuln biomass to converte catch to numbers
          uAge_atgp(a,t-1,gIdx,p) = vulnB_atgp(a,t-1,gIdx,p) / vulnB_tgp(t-1,gIdx,p);

          // Get numbers caught at age
          catAge_atgp(a,t-1,gIdx,p) = uAge_atgp(a,t-1,gIdx,p) * C_tgp(t-1,gIdx,p) / wt(a);
        }
        // Calculate F - there might be a better way here...
        // Read MacCall's paper on alternatives to Pope's approx
        // Question is: what do we use for estimating F? Just U? Crank this
        // on paper first.
        // Pope's approx works better within a single age class, or
        // with DD models (F = log(Nt/Nt-1)-M)
        F_tgp(t-1,gIdx,p) = C_tgp(t-1, gIdx,p) / vulnB_tgp(t-1,gIdx,p) ;

        // Remove numbers from the total population
        // SimulFleets //
        // Here is where we'll need to take care of
        // the vulnerable biomass for simultaneous fleet timings
        // 1. Check if the next fleet's timing is the same as this
        // fleet's
        // 2. If so, use the previous numbers to
        // calculate vuln biomass at the same timing
        for( int a = 0; a < nA; a++)
        {
          tmpN_at(a) -= catAge_atgp(a,t-1,gIdx,p);
          tmpN_at(a) =  posfun(Type(tmpN_at(a)), Type(1e-3), posPen );
        }
        prevTime = fleetTiming(gIdx);
      }

      // Finally, advance numbers at age
      // to the following time step by reducing
      // remaining numbers by the remaining mortality,
      // and adding recruitment
      lastFrac = 1 - prevTime;
      for(int a = 0; a < nA; a++ )
      {
        // Recruitment, eqn A.2
        if( a == 0 )
        {
          N_atp(a,t,p) = reca_p(p)*SB_pt(p,t-1)/( 1. + recb_p(p)*SB_pt(p,t-1) );
            N_atp(a,t,p) *= exp(omegaR_tp(t-1,p));
          R_pt(p,t) = N_atp(a,t,p);
        } else {
          // Depletion by lastFrac*Mt
          N_atp(a,t,p) = tmpN_at(a-1) * exp( - lastFrac * M_pt(p,t-1));  
          if( a == nA - 1)
          {  
            // Use same M for plus group
            if(useMsen == 0)
              N_atp(nA-1,t,p) += tmpN_at(nA-1) * exp( - lastFrac * M_pt(p,t-1));

            // Add senescence M
            if(useMsen == 1)
              N_atp(nA-1,t,p) += tmpN_at(nA-1) * exp( - lastFrac * Msen);

          }  
        }
        // Convert to biomass
        B_atp(a,t,p) = N_atp(a,t,p) * wt(a);
      }

      // Caclulate spawning biomass at beginning of next time step
      SB_pt(p,t) = (B_atp.col(p).col(t) * mat).sum();

    } // End of population dynamics
  } // End of stock specific pop dynamics - could potentially extend this loop to cover observation models 
  
  // Calculate likelihood functions
  // Observation models //
  
  // Initialise lnqhat and lntauObs at 0
  lnqhat_gp.fill(0.0);
  lntauObs_gp.fill(0.0);
  z_tgp.fill(0.0);
  zSum_gp.fill(0.0);
  validObs_gp.fill(0);
  etaSumSq_gp.setZero();
  nResids_gp.setZero();
  nObsAge_gp.setZero();
  // Loop over stocks and gear types
  for( int p = 0; p < nP; p++)
  {
    for( int gIdx = 0; gIdx < nG; gIdx++ )
    {
      validObs_gp(gIdx,p) = int(0);
      // Stock indices (concentrate obs error var and lnq)
      // Check if this is a survey
      if( calcIndex_g(gIdx) == 1)
      {
        Type idxState = 0.;
        // Loop over time steps
        for( int t = 0; t < nT; t++ )
        {
          if( I_tgp(t,gIdx,p) > 0.0 )
          {
            // Recover numbers or biomass
            if( survType_g(gIdx) == 1 )
              idxState = vulnN_atgp.col(p).col(gIdx).col(t).sum();
            if( survType_g(gIdx) == 0 )
              idxState = vulnB_tgp(t,gIdx,p);

            // Calculate residual
            z_tgp(t,gIdx,p) = log(I_tgp(t,gIdx,p)) - log(idxState);
            // Add to sum of residuals
            zSum_gp(gIdx,p) += z_tgp(t,gIdx,p);
            validObs_gp(gIdx,p) += int(1);
          }
        }
        SSR_gp(gIdx,p) = 0.;
        // Calculate conditional MLE of q
        // if a relative index
        if( indexType_g(gIdx) == 0 & validObs_gp(gIdx,p) > 0 )
        {
          // mean residual is lnq (intercept)
          lnqhat_gp(gIdx,p) = zSum_gp(gIdx,p) / validObs_gp(gIdx,p);

          // Subtract mean from residuals for
          // inclusion in likelihood
          for(int t = 0; t < nT; t++)
            if( I_tgp(t,gIdx,p) > 0.0)
            {
              z_tgp(t,gIdx,p) -= lnqhat_gp(gIdx,p);
              // Sum squared resids
              SSR_gp(gIdx,p) += square(z_tgp(t,gIdx,p));    
            }
        }
        if( validObs_gp(gIdx,p) > 0)
        {
          // Calculate conditional MLE of observation
          // index variance
          tauObs_gp(gIdx,p) = sqrt(SSR_gp(gIdx,p)/validObs_gp(gIdx,p));

          // Add concentrated nll value using cond MLE of tauObs
          obsIdxNLL_gp(gIdx,p) += 0.5*(validObs_gp(gIdx,p) * log( SSR_gp(gIdx,p) / validObs_gp(gIdx,p) ) + validObs_gp(gIdx,p));
        }
      }


      // Age observations //
      // Loop over time steps
      for( int t = 0; t < nT; t++ )
      { 
        // Check that age observations
        // exist by checking that the plus
        // group has observations
        if( A_atgp(nA-1,t,gIdx,p) >= 0 )
        {
          Type         sumPropAge = 0.;
          // Now estimate predicted catch-at-age
          // This was done already if catch > 0, but 
          // not for all fleets (fishery indep. surveys
          // would be missed)
          // First, calculate prop at age in each fleet
          for(int a = 0; a < nA; a++ )
          {
            predPA_atgp(a,t,gIdx,p)   = uAge_atgp(a,t,gIdx,p);
            // Convert to numbers
            predPA_atgp(a,t,gIdx,p)   /= wt(a);  
            sumPropAge                += predPA_atgp(a,t,gIdx,p);
          }

          // Save to array and renormalise
          predPA_atgp.col(p).col(gIdx).col(t) /= sumPropAge;

          vector<Type> obsAge = A_atgp.col(p).col(gIdx).col(t);
          vector<Type> predAge = predPA_atgp.col(p).col(gIdx).col(t);

          predAge = Q * predAge;
          predAge /= predAge.sum();
          predPA_atgp.col(p).col(gIdx).col(t) = predAge;


          // Calculate logistic normal likelihood components
          // if age observations exist this year
          if( A_atgp(0,t,gIdx,p) >= 0)
          {
            ageResids_atgp.col(p).col(gIdx).col(t) = calcLogistNormLikelihood_tc( obsAge, 
                                                                                  predAge,
                                                                                  minPropAge,
                                                                                  etaSumSq_gp(gIdx,p),
                                                                                  nResids_gp(gIdx,p) );
            nObsAge_gp(gIdx,p) += 1;
          }
          // Add contribution to age comps likelihood
          if( nResids_gp(gIdx,p) > 0)
          {
            tau2Age_gp(gIdx,p)    += etaSumSq_gp(gIdx,p) / nResids_gp(gIdx,p);
            ageObsNLL_gp(gIdx,p)  += ageCompWeight_g(gIdx) * 0.5 * (nResids_gp(gIdx,p) - nObsAge_gp(gIdx,p)) * log(tau2Age_gp(gIdx,p));
          }
        }
      }
    }
  }


  // Now add contribution of mixed age
  // compositions to the likelihood

  // Age observations
  vector<Type> etaSumSq_g(nG);
  vector<Type> nResids_g(nG);
  vector<Type> nObsAge_g(nG);
  vector<Type> tau2Age_g(nG);
  vector<Type> ageObsNLL_g(nG);
  etaSumSq_g.setZero();
  nResids_g.setZero();
  nObsAge_g.setZero();
  tau2Age_g.setZero();
  ageObsNLL_g.setZero();
  ageResMix_atg.setZero();

  array<Type>   predMixedPA_atg(nA,nT,nG);
  predMixedPA_atg.fill(-1);

  for( int gIdx = 0; gIdx < nG; gIdx++ )
  {
    // Check that mixed age comps should be calculated
    if( mixedAgeComp_g(gIdx) == 1)
    {
      
      // Age observations //
      // Loop over time steps
      for( int t = 0; t < nT; t++ )
      { 
        // Check that age observations
        // exist by checking that the plus
        // group has non-negative value (negative => missing)
        if( mA_atg(nA-1,t,gIdx) >= 0 )
        {
          // Initialize as zero
          predMixedPA_atg.col(gIdx).col(t).fill(0.0);

          // Now estimate predicted catch-at-age
          // This was done already if catch > 0, but 
          // not for all fleets (fishery indep. surveys
          // would be missed)

          // First, check if catAge 4 is > 0 for any stocks
          // in this fleet at this time step (choosing age
          // 4 as this is a selected age)
          array<Type> checkCatAge_pa(nP,nA);
          checkCatAge_pa = catAge_atgp.rotate(1).col(gIdx).col(t);

          vector<Type> checkCatAge_p(nP);
          checkCatAge_p.setZero();
          for( int p = 0; p < nP; p++)
            checkCatAge_p = checkCatAge_pa.transpose().col(p).sum();

          if( checkCatAge_p.sum() > 0 )
          {
            // make predicted proportions at age
            // from the sum of the catch-at-age
            for( int p = 0; p < nP; p++ )
              predMixedPA_atg.col(gIdx).col(t) += catAge_atgp.col(p).col(gIdx).col(t);
          }

          // Otherwise, we take the average prop, weighted
          // by vulnerable numbers at age
          if( checkCatAge_p.sum() == 0 )
          {
            for( int a = 0; a < nA; a++ )
              for( int p = 0; p < nP; p++ )
                predMixedPA_atg(a,t,gIdx) += uAge_atgp(a,t,gIdx,p) * vulnN_atgp(a,t,gIdx,p) / mixedVulnN_atg(a,t,gIdx);

          }

          // Save to array and renormalise
          predMixedPA_atg.col(gIdx).col(t) /= predMixedPA_atg.col(gIdx).col(t).sum();  

          vector<Type> obsAge = mA_atg.col(gIdx).col(t);
          vector<Type> predAge = predMixedPA_atg.col(gIdx).col(t);

          predAge = Q * predAge;
          predAge /= predAge.sum();
          predMixedPA_atg.col(gIdx).col(t) = predAge;

          // Calculate logistic normal likelihood components
          // if age observations exist this year
          ageResMix_atg.col(gIdx).col(t) = calcLogistNormLikelihood_tc( obsAge, 
                                                                        predAge,
                                                                        minPropAge,
                                                                        // minAge,
                                                                        etaSumSq_g(gIdx),
                                                                        nResids_g(gIdx) );
          nObsAge_g(gIdx) += 1;

        }
      }
      // Add contribution to age comps likelihood
      if( nResids_g(gIdx) > 0)
      {
        tau2Age_g(gIdx)    += etaSumSq_g(gIdx) / nResids_g(gIdx);
        ageObsNLL_g(gIdx)  += ageCompWeight_g(gIdx) * 0.5 * (nResids_g(gIdx) - nObsAge_g(gIdx)) * log(tau2Age_g(gIdx));
      }
    }
  }  


  // Process error priors
  // Recruitment priors
  // Add recruitment deviations to rec NLL; sigmaR is estimated
  for( int p = 0; p < nP; p ++ )
  {
    for( int t = firstRecDev-1; t < lastRecDev; t++)
      recNLP -= dnorm( omegaR_tp(t,p),Type(0),sigmaR,true);

    for( int a = 0; a < nA; a++)
      recNLP -= dnorm(log_initN_ap(a,p), Type(0), sigmaR, true);

  }
  // Beta prior on steepness
  Type steepness_nlp  = (1 - rSteepBetaPrior(0)) * log(rSteepness) + (1 - rSteepBetaPrior(1)) * log(1-rSteepness);
  // Add a prior on the steepness stock effects
  steepness_nlp -= dnorm( epsSteep_p, Type(0), sigmaSteepp, true).sum();
  // Add a recruitment var IG prior
  Type sig2RNLP = (sig2RPrior(0)+Type(1))*2*lnsigmaR + sig2RPrior(1)/square(sigmaR);

  // Natural mortality
  // Natural mortality prior
  mortNLP -= dnorm( lnM, log(initMPrior(0)),initMPrior(1), true);

  // Mt deviations; sigmaM is estimated
  for(int p = 0; p < nP; p++)
    for( int t = 0; t < nT - 1; t++)
      mortNLP -= dnorm( omegaM_pt(p,t), Type(0), sigmaM, true);

  mortNLP -= dnorm( epsM_p, Type(0), sigmaMp, true).sum();

  // Add mortality var IG prior
  Type sig2MNLP = (sig2MPrior(0)+Type(1))*2*lnsigmaM + sig2MPrior(1)/square(sigmaM);

  // Add gear-wise priors:
  // Catchability, index obs error variance,
  // and a prior on selectivity pars
  vector<Type> selModeNLP_g(nG);
  selModeNLP_g.setZero();
  array<Type> selAlphaDevNLP_gp(nG,nP);
  array<Type> selBetaDevNLP_gp(nG,nP);
  selAlphaDevNLP_gp.setZero();
  selBetaDevNLP_gp.setZero();

  // Solve for IG beta parameters
  vector<Type> obstau2IGb(nG);
  obstau2IGb = calcIGbeta( obstau2IGmode, obstau2IGa);

  for( int gIdx = 0; gIdx < nG; gIdx++ )
  {
    // Catchability
    for( int p=0; p < nP; p++)
    {
      qhat_gp(gIdx,p) = exp(lnqhat_gp(gIdx,p));
      qNLP -= dnorm( lnqhat_gp(gIdx,p), log(mq(gIdx)), sdq(gIdx), true);
    
      // IG prior on survey obs err variance
      if( calcIndex_g(gIdx) == 1 & validObs_gp(gIdx,p) > 0  )
        nlptau2idx_gp(gIdx,p) += dinvgamma( tauObs_gp(gIdx,p),
                                            obstau2IGa(gIdx),
                                            obstau2IGb(gIdx),
                                            1 );

      // Add time-varying selectivity deviations to prior
      for( int t = 0; t < nT; t++)
      {
        selAlphaDevNLP_gp(gIdx,p) -= dnorm(epsSelAlpha_gtp(gIdx,t,p), Type(0), sigmaSelAlpha_g(gIdx),true );
        selBetaDevNLP_gp(gIdx,p) -= dnorm(epsSelBeta_gtp(gIdx,t,p), Type(0), sigmaSelBeta_g(gIdx),true );
      }
    }
    // mean gear selectivity mode/95%
    selModeNLP_g(gIdx) += pow((selModeX_g(gIdx) - mSelMode_g(gIdx))/(selModeCV*mSelMode_g(gIdx)),2 );
    
  }
  
  // Add positive function penalty to objFun
  objFun += posPenFactor * posPen;

  // Add Jeffrey's prior to B0
  Type B0nlp = 0;
  B0nlp += lnB0_p.sum();

  // Turn on/off selectivity prio (0=OFF, 1=ON)
  if(selPriorOn == 0)
    selModeNLP_g.setZero();

  // Add NLL and NLP contributions to objFun
  // BD (2019-10-10): removed qNLP from objective function for OMs
  // BD (2019-11-07): removed selModeNLP from objective function for OMs
  objFun += mortNLP + 
            recNLP + 
            steepness_nlp +
            obsIdxNLL_gp.sum() + 
            ageObsNLL_gp.sum() + 
            ageObsNLL_g.sum() + 
            // qNLP + 
            nlptau2idx_gp.sum() +
            selModeNLP_g.sum() +
            selAlphaDevNLP_gp.sum() +
            selBetaDevNLP_gp.sum() +
            B0nlp;

  // Convert some values to log scale
  // for sd reporting
  array<Type> lnSB_pt(nP,nT);
  array<Type> lnD_pt(nP,nT);
  array<Type> lnR_pt(nP,nT);
  array<Type> lnM_pt(nP,nT);
  // Fill arrays
  for( int t = 0; t < nT; t++)
  {
    lnSB_pt.col(t)  = log(SB_pt.col(t));
    lnD_pt.col(t)   = log(SB_pt.col(t)) - lnB0_p;
    lnR_pt.col(t)   = log(R_pt.col(t));
    lnM_pt.col(t)   = log(M_pt.col(t));
  }
  vector<Type> lnM_p          = log(M_p);


  /*\/\/\/\/\ REPORTING SECTION /\/\/\/\*/
  // Variables we want SEs for
  ADREPORT(lnSB_pt);
  ADREPORT(lnR_pt);
  ADREPORT(lnM_pt);
  ADREPORT(lnM_p);
  // // ADREPORT(lnF_tgp);
  ADREPORT(lnqhat_gp);
  ADREPORT(lnD_pt);
  ADREPORT(rSteepness_p);
  ADREPORT(rSteepness);


  
  // Everything else //

  // Leading pars
  REPORT(B0_p);                   // Unfished biomass
  REPORT(M_p);                    // Initial/constant natural mortality
  REPORT(M);                      // Species M
  REPORT(Msen);                   // Senescence M for plus group
  REPORT(rSteepness);             // Species steepness
  REPORT(initN_mult_ap);          // Initial numbers multiplier (fished)
  REPORT(rSteepness_p);           // Steepness
  REPORT(sigmaM);                 // Mt devations sd
  REPORT(sigmaR);                 // Recruitment deviations SD

  // Growth and maturity schedules
  REPORT(mat);
  REPORT(wt);
  REPORT(len);
  REPORT(age);
  REPORT(surv_pa);

  // Model dimensions
  REPORT(nT);
  REPORT(nG);
  REPORT(nA);
  REPORT(nP);

  // Selectivity
  REPORT(sel_ag);
  REPORT(sel_agtp);
  REPORT(SelAlpha_g);
  REPORT(SelBeta_g);
  REPORT(SelAlpha_gtp);
  REPORT(SelBeta_gtp);
  REPORT(epsSelAlpha_gtp);
  REPORT(epsSelBeta_gtp);
  REPORT(sigmaSelAlpha_g);
  REPORT(sigmaSelBeta_g);
  REPORT(selModeX_g);
  REPORT(selLen_g);

  // SR Variables
  REPORT(R0_p);
  REPORT(reca_p);
  REPORT(recb_p);
  REPORT(phi_p);

  // State variables
  REPORT(B_atp);
  REPORT(N_atp);
  REPORT(vulnB_tgp);
  REPORT(vulnB_atgp);
  REPORT(vulnN_tgp);
  REPORT(vulnN_atgp);
  REPORT(mixedVulnN_atg);
  REPORT(SB_pt);
  REPORT(R_pt);
  REPORT(M_pt);
  REPORT(F_tgp);

  // Fleet reordering and 
  // Pope's approx
  REPORT(chronIdx);
  REPORT(usedFleet);
  REPORT(fleetTiming);
  REPORT(uAge_atgp);
  REPORT(catAge_atgp);
  REPORT(predPA_atgp);
  REPORT(fracM);
  REPORT(lastFrac);

  // Data switches
  REPORT(survType_g);     // Type of index (0 = vuln bio, 1 = vuln numbers)
  REPORT(indexType_g);    // Type of survey (0 = relative, 1 = absolute)
  REPORT(calcIndex_g);    // Calculate fleet index (0 = no, yes = 1)
  REPORT(selType_g);      // Type of selectivity (0 = asymptotic, 1 = domed (normal))
  REPORT(minPropAge);     // Min prop'n at age to avoid accumulation in L-N age comp likelihood
  
  // Data
  REPORT(C_tgp);
  REPORT(I_tgp);
  REPORT(A_atgp);
  REPORT(mA_atg);
  REPORT(Q);

  // Random effects
  REPORT(omegaM_pt);
  REPORT(omegaR_tp);
  REPORT(recDevs_vec);
  
  // Observation model quantities
  REPORT(qhat_gp);
  REPORT(tauObs_gp);
  REPORT(z_tgp);
  REPORT(zSum_gp);
  REPORT(validObs_gp);
  REPORT(SSR_gp);
  REPORT(etaSumSq_gp);
  REPORT(tau2Age_gp);
  REPORT(nResids_gp);
  REPORT(nObsAge_gp);  

  // Mixed observations
  REPORT(etaSumSq_g);
  REPORT(tau2Age_g);
  REPORT(nResids_g);
  REPORT(nObsAge_g);
  REPORT(predMixedPA_atg);

  // Switches and weights
  REPORT(ageCompWeight_g);
  REPORT(mixedAgeComp_g);

  // Likelihood/prior values
  REPORT(objFun);
  REPORT(ageObsNLL_gp);
  REPORT(ageObsNLL_g);
  REPORT(obsIdxNLL_gp);
  REPORT(recNLP);
  REPORT(mortNLP);
  REPORT(qNLP);
  REPORT(selNLP_gp);
  REPORT(sig2MNLP);
  REPORT(sig2RNLP);
  REPORT(nlptau2idx_gp);
  REPORT(steepness_nlp);
  REPORT(posPen);
  REPORT(selModeNLP_g);
  REPORT(selAlphaDevNLP_gp);
  REPORT(selBetaDevNLP_gp);
  
  return objFun;
}
