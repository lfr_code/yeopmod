Hi Dana,
 
The first attached zip contains the last 2015 catch reconstruction that I believe was used for the ResDoc. There are 6 tables of reconstructed Yelloweye Rockfish (YYR) catch by fishery -- Trawl, Halibut, Sablefish, Dogfish|Lingcod, H&L Rockfish, and All Combined. Each table shows annual catch by calendar year (rows) and by major PMFC area code (1=4B, 3=3C, 4=3D, 5=5A, 6=5B, 7=5C, 8=5D, 9=5E) in columns. The 2015 zip also contains the estimated gamma ratios (YYR/ORF) by area and fishery and discard ratios delta.
 
Over the years, we've refined the catch reconstruction as new issues pop up. I've attached a zip of a 2018 YYR reconstruction I did this morning.
As in 2015:
·         The reconstruction (use of gamma) occurs prior to 1979 for trawl, 1982 for halibut, 1996 for sablefish, 1982 for dogfish/lingcod, and 1982 for H&L rockfish;
·         Gamma for halibut (fishery 2) was set to 25% in 5AB, 37.5% in 5C, and 30% in 5D (Chris Sporer);
·         Foreign catches (1965-1976) were not used in the total ORF, i.e., we assume no catch of YYR by offshore fleets from Russia and Japan (2015 workshop);
·         Langara Spit experiment catches of ORF were not used to calculate YYR (2015 workshop);
·         Seamount records were excluded;
·         No discards were calculated for the trawl fleet (Brian Mose).
Unlike 2015:
·         Area-fishery gamma and delta were calculated using a geometric mean of non-zero annual ratios;
·         Gamma and delta were NOT depth-stratified.
 
Feel free to contact me if you need other scenarios.
 
Cheers, Rowan
Tel. (250) 363-8718
 
 