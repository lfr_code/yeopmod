Methods - estRecYE_NorthSouth_prelimVals.csv

outside_t 1918-2014: median reconstructed rec YE catch used in 2014 stock assessment (Table A.12)

outside_t 2015-2018: totals provided by DFO for 2015 (SFAB Groundfish Shellfish Working Group Presentation Feb 7, 2019, slide 11) and 2016-2018 (pers comm, S. Petersen, DFO)

south_t 2000-2018: sum of creel survey catch for WCVI for 2000-2018 

north_t 2000-2018: outside_t - south_t for 2000-2018

north_t & south_t for 1918-1999: assumes 67% of outside_t catch occurs in north for 1918-1999 based on 2000-2018 avg proportion in north/south (needs input from DFO/industry)


Notes from 2018 Rec Estimates Slides (SFAB Groundfish Shellfish Working Group Presentation Feb 7, 2019):

- Kept pieces only recorded for HG and CC lodge survey in PFMAs 1,2 and 7,8,9
- Kept and released pieces recorded in North Coast creel survey and WCVI creel surveys in PFMAs 3,4 and WCVI PFMAs
- Ratio of releases to discards is 0.21 and 0.16 for WCVI and NC respectively (slide 25)
- Average ratio of 0.19 used to estimate releases for lodge survey areas in HG and CC in PFMAs 1,2, and 7,8,9
