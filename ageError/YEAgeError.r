# Develop age-error matrix for Yelloweye using duplicate age reads, using Naive normal approach from Richards et al. 1992 Heifetz et al. 1999

library(dplyr)

setwd('~/Documents/LANDMARK/PHMA/2018_YEassessment/yeopmod/ageError')

source('ageErrFuns.R')

dat <- read.csv('YE_age_precision_OUT.csv')

# remove NAs
dat <- subset(dat, !(is.na(fin_specimen_age)))

# Calculate ageing-error, assuming Final_age column is the correct age
dat$readerDif <- dat$prim_specimen_age - dat$sec_specimen_age
dat$Finaldif1 <- dat$prim_specimen_age - dat$fin_specimen_age
dat$Finaldif2 <- dat$sec_specimen_age - dat$fin_specimen_age
dat$meanFinalDif <- (dat$Finaldif1 + dat$Finaldif2)/2
dat$meanReaderAge <- (dat$prim_specimen_age + dat$sec_specimen_age)/2

# round mean reader age to nearest even integer
dat$meanReaderAge <- round(dat$meanReaderAge)

# subset males and females
dat.mf <- dat
dat.m <- subset(dat, sex==1)
dat.f <- subset(dat, sex==2)

# Identify cases where neither age was 'correct'
rows <- which(dat$readerDif!=0 
              & dat$Finaldif1!=0 & dat$Finaldif2!=0 )
head(dat[rows,])
cat(nrow(dat[rows,]), 'where neither age reading was correct')


###############################################
# Fit naive normal model for age-error matrix #
###############################################

# -- negLL() --

# Implement ageing-error matrix likelihood eqn as described in Richards et al. 1992 and Heifetz et al. 1999

negLL <- function(pars, estList = estList, fit=T)
{

  # Get information from estList
  phi       <- estList$phi
  fnScale   <- estList$fnScale

  aij <- phi$aij
  bi <- phi$bi
  ages <-1:phi$A

  # Get pars from theta
  sigma1   <- exp(pars[1])
  sigmaA   <- exp(pars[2])
  alpha    <- pars[3]

  # STATISTICAL MODEL FOR AGEING ERROR
  # Eqn numbers from Heifetz et al. 1999

  # Eqn 1: sd of observed age for true age b
  sigma <- vector(length=phi$A)
  for (b in 1:phi$A)
  {  
    if(alpha !=0)
    {  
      num <- 1- exp(-alpha*(b-1))
      denom <- 1- exp(-alpha*(phi$A-1))
      sigma[b] <- sigma1 + (sigmaA - sigma1)*num/denom
    }

    if(alpha ==0)
    {  
      num <- b-1
      denom <- phi$A-1
      sigma[b] <- sigma1 + (sigmaA - sigma1)*num/denom
    }
  }

  #Eqn 2: for each fish and reader, calculate qab
  qij <- matrix(nrow=nrow(aij), ncol=ncol(aij))

  for (i in 1:nrow(aij))
  {
      for (j in 1:ncol(aij))
      {  
        # discrete normal density function
        a <- aij[i,j]  # observed age by primary reader
        b <- bi[i]
        sigma_b <- sigma[b] 

        # numerator
        term1 <- ((a-b)/sigma_b)^2
        num.Xab <- exp(-.5*term1)/sqrt(2*pi*sigma_b^2)

        if(!is.finite(num.Xab))
        browser()
        
        # Assign very small number instead of 0
        if(num.Xab==0)
        {  
          # browser()
          num.Xab <- exp(-740)/sqrt(2*pi*sigma_b^2)
        }  

        # denom
        b <- rep(b, length(ages))
        sigma_b <- rep(sigma_b, length(ages) )
        
        term1 <- ((ages-b)/sigma_b)^2
        
        # Assign very small number instead of 0
        term1[term1==0] <- exp(-740)/sqrt(2*pi*sigma_b[1]^2)

        denom.Xab <- sum(exp(-.5*term1)/sqrt(2*pi*sigma_b^2))

        qij[i,j] <- num.Xab/denom.Xab

      }  

  } 
  
    # Eqn 6&7. Likelihood from Heifetz et al. 1999
    negLL <- -1* sum( colSums(log(qij))  ) 

    negLL <- fnScale * negLL

    if( !(is.finite(negLL)))       
    {
      negLL <- 10.^10. 
    } 

    # If statement below controls how much information is returned when using negLL function
    # To get parameter estimates using optim(), we want to return only the negative log likelihood value that is being minimized by optim(). Otherwise, we want all estimates 
    
    # fit = TRUE returns negLL value only
    # fit = FALSE returns list of pars & negLL

   if (fit)
   {
      return(negLL)
   }
   else 
   {
    # create list to hold results
    result <- list()
      
    result$pars <- c(sigma1 = sigma1,
                     sigmaA = sigmaA,
                     alpha = alpha)
    result$negLL <- negLL
    
    

    return(result)
   }


} # END negLL()


# -- genPhi()---

# specifiy data inputs for model:
# - maxTrueAge: max true Age for data used in model fitting
# - trueAgeType: meanReaderAge or FinalAge
# - plusGroup: T/F, reassign observations to plusGroup

genPhi <- function(data, maxTrueAge, trueAgeType, 
                   plusGroup=F)
{
  
  # re-assign all ages> maxTrueAge to maxTrueAge
  if(plusGroup)
  {
    rows <- which(data$prim_specimen_age > maxTrueAge)
    data$prim_specimen_age[rows] <- maxTrueAge
    rows <- which(data$sec_specimen_age > maxTrueAge)
    data$sec_specimen_age[rows] <- maxTrueAge
    rows <- which(data$fin_specimen_age > maxTrueAge)
    data$fin_specimen_age[rows] <- maxTrueAge
    rows <- which(data$meanReaderAge > maxTrueAge)
    data$meanReaderAge[rows] <- maxTrueAge
    dataset <- data
    A <- maxTrueAge
  } else
  {
  # Set max age and truncate database for actual ages
    if(trueAgeType == 'meanReaderAge')
    dataset <- subset(data, meanReaderAge <=maxTrueAge)

    if(trueAgeType == 'FinalAge')
    dataset <- subset(data, fin_specimen_age <=maxTrueAge)

    A <- max(dataset$prim_specimen_age, dataset$sec_specimen_age)
  }

  # for i fish
  aij <- matrix(nrow=nrow(dataset), ncol=2)
  aij[,1] <- dataset$prim_specimen_age
  aij[,2] <- dataset$sec_specimen_age
  
  if(trueAgeType == 'meanReaderAge')
  bi <- dataset$meanReaderAge  

  if(trueAgeType == 'FinalAge')
  bi <- dataset$fin_specimen_age 

  # Specify Design Vector
  phi <- list(aij=aij, bi=bi, A=A)

  #Create List of negLL arguments
  estList <- list(fnScale = 1, phi = phi,  maxit=500,
                  ageType=trueAgeType, maxTrueAgeFit=maxTrueAge)

  return(estList)
}  # End genPhi()


# OPTIMIZE PAR ESTIMATES

# Set initial par values
sigma1 <- 1
sigmaA <- 4
alpha <- 0

theta <- c(log(sigma1), log(sigmaA), alpha)

# Optim will minimize negLL (i.e. maximize log likelihood) using Newton's law and iteration

# CASE 1: True Age = mean reader age, max true age = 119, both sexes
estList1 <- genPhi(data=dat.mf, 119, 'meanReaderAge')

# CASE 2: True Age = Final Age, max true age = 119, both sexes
estList2 <- genPhi(data=dat.mf, 119, 'FinalAge')

# CASE 3: True Age = mean reader age, max true age = 119, only males
estList3 <- genPhi(data=dat.m, 119, 'meanReaderAge')

# CASE 4: True Age = Final Age, max true age = 119, only males
estList4 <- genPhi(data=dat.m, 119, 'FinalAge')

# CASE 5: True Age = mean reader age, max true age = 119, only females
estList5 <- genPhi(data=dat.f, 119, 'meanReaderAge')

# CASE 6: True Age = Final Age, max true age = 119, only females
estList6 <- genPhi(data=dat.f, 119, 'FinalAge')

# CASE 7: True Age = mean reader age, max true age = 65, both sexes
estList7 <- genPhi(data=dat.mf, 65, 'meanReaderAge')

# CASE 8: True Age = Final Age, max true age = 65, both sexes
estList8 <- genPhi(data=dat.mf, 65, 'FinalAge')

estListList <- list(estList1=estList1, estList2=estList2,
                    estList3=estList3, estList4=estList4,
                    estList5=estList5, estList6=estList6,
                    estList5=estList7, estList8=estList8)

mleList <- list()
for (i in 1:length(estListList))
{

est <- estListList[[i]]
parEst <- optim(par = theta, fn = negLL, estList = est, 
                method='L-BFGS-B', hessian = TRUE,
                lower= c(-5,-5,-2), upper=c(5,5,2))
# lower= c(-Inf,-Inf,-10), upper=c(Inf,Inf,10))
# exp(parEst$par[1]); exp(parEst$par[2]); parEst$par[3] 

mle <- negLL(pars=parEst$par, estList= est, fit=F )
mle$case <- i
mle$estList <- est

mleList[[i]] <- mle

}

# Save R data file
save(mleList, file="mleList.Rdata")

# Generate age-error matrices
ageErrMat <- genAgeErrMat(cases=1:2,save.csv=TRUE)

# Generate normalized age-error matrices for plus group
pg <- 65

# Method 1 - use probability for min. age in plus group
mat_minPG <- array(dim=c(pg,pg,2))
for(m in 1:2)
mat_minPG[,,m] <- normalizeAgeErrMat(ageErrMat= ageErrMat,
                                     case=m,method='minPG')

# Method 2 - weighted by natural mortality
mat_wM <- array(dim=c(pg,pg,2))
for(m in 1:2)
mat_wM[,,m] <- normalizeAgeErrMat(ageErrMat= ageErrMat,
                                     case=m,method='wM')

# Method 3 - mean of all plus group ages
mat_mean <- array(dim=c(pg,pg,2))
for(m in 1:2)
mat_mean[,,m] <- normalizeAgeErrMat(ageErrMat= ageErrMat,
                                     case=m,method='mean')
##################
# Generate Plots #
##################

clrs <- c('#66c2a5','#fc8d62','#8da0cb','#e78ac3',
          '#a6d854','#ffd92f', '#e5c494', '#b3b3b3')

lType <-rep(c(1,3),4)

caseNames =c('Case 1: True Age = Mean Reader Age',
             'Case 2: True Age = Final Age Assigned',
             'Case 3: True Age = Mean Reader Age, Males Only',
             'Case 4: True Age = Final Age Assigned, Males only',
             'Case 5: True Age = Mean Reader Age, Females Only',
             'Case 6: True Age = Final Age Assigned, Females only',
             'Case 7: True Age = Mean Reader Age, Max Age=65',
             'Case 8: True Age = Final Age Assigned, Max Age=65')        

plotPars <- list(clrs=clrs,
                 lType=lType,
                 caseNames=caseNames)

# Plot fits for all cases
png("figs/agingErrFits_allCases.png",
    width=700, height=700)
plotAgeErrFits(cases=1:8, plotPars, mleList=mleList)
dev.off()

# Plot fits for cases 1-2
png("figs/agingErrFits_cases1&2.png",
    width=700, height=700)
plotAgeErrFits(cases=1:2, plotPars, mleList=mleList)
dev.off()

# Plot probality of observed ages given true age
png("figs/agingErrMats_noNormalization.png",
    width=700, height=700)
plotAgeErrMat(cases=1:2, ageErrMat=ageErrMat, obsAges=1:80)
dev.off()

# Plot for normalized age-error matrix using min PG
png("figs/agingErrMats_minPG.png",
    width=700, height=700)
plotAgeErrMat(cases=1:2, ageErrMat=mat_minPG, obsAges=1:65)
dev.off()

# Plot for normalized age-error matrix using weighted M
png("figs/agingErrMats_wM.png",
    width=700, height=700)
plotAgeErrMat(cases=1:2, ageErrMat=mat_wM, obsAges=1:65)
dev.off()

# Plot for normalized age-error matrix using mean
png("figs/agingErrMats_mean.png",
    width=700, height=700)
plotAgeErrMat(cases=1:2, ageErrMat=mat_mean, obsAges=1:65)
dev.off()






