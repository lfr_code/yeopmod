# functions for YEageError.R

# -- normalizeAgeErrMat() --

normalizeAgeErrMat <- function(ageErrMat, case,
                               M=0.0345, plusGroup=65,
                               method, save.csv=TRUE)
{
  
  # assign zero for any values <0.0005 and renormalize
  mat <- ageErrMat[,,case]
  mat <- as.matrix(mat)
  mat[mat<0.0005] <- 0

  # renormalize
  colSums <- colSums(mat)
  for(i in 1:ncol(mat))
  {  
    mat[,i] <- mat[,i]/colSums[i]
  }  
  print(colSums(mat))

  # sum all probs into plusgroup
  pMat <- mat
  pMat[plusGroup,] <- colSums(pMat[plusGroup:nrow(pMat),] )
  pMat <- pMat[1:plusGroup,]
  colSums(pMat)

  # Method 1 - assume probability of observed ages for plus group is equal to probability of observed ages for last age in matrix (i.e 65 years if max age=65)
  if(method=='minPG')
  {  
    newMat <- pMat[,1:plusGroup]
    filename <- paste('ageErrMats/ageErrMatCase',case,
                      'pg',plusGroup,'_minPG.csv', sep='')
  }  

  # Method 2 - assume probability of observed ages for plus group is weighted average of plus group ages according to natural mortality
  if(method=='wM')
  { 
    nAge <- vector('integer', length=length(plusGroup:ncol(mat)))
    nAge[1] <- 1
    for(i in 1:length(nAge))
      nAge[i] <- nAge[1]*exp(-M)^(i-1)
    w <- nAge/sum(nAge)

    plusCol <- pMat[,plusGroup:ncol(mat)]
    for (i in 1:length(w))
      plusCol[,i] <- w[i]*plusCol[,i]

    plusCol <- rowSums(plusCol)
    sum(plusCol)

    newMat <- pMat[,1:plusGroup]
    newMat[,plusGroup] <- plusCol

    filename <- paste('ageErrMats/ageErrMatCase',case,
                      'pg',plusGroup,'_wM.csv', sep='')
  }


  # Method 3 - assume probability of observed ages for plus group is average of plus group ages
  if(method=='mean')
  { 
    plusCol <- pMat[,plusGroup:ncol(mat)]

    plusCol <- rowSums(plusCol)/ncol(plusCol)
    sum(plusCol)

    newMat <- pMat[,1:plusGroup]
    newMat[,plusGroup] <- plusCol

    filename <- paste('ageErrMats/ageErrMatCase',case,
                      'pg',plusGroup,'_mean.csv', sep='')
  }

  if(save.csv)
  write.csv(newMat, filename, row.names=F)

  return(newMat)
} # END normalizeAgeErrMat()


# -- genAgeErrMat() --

genAgeErrMat <- function(cases= 1:length(mleList),
                         maxA=119, save.csv=TRUE)
{
  ageErrMat_m <- array(dim=c(maxA,maxA,length(cases)))

  for (m in cases)
  {  

    sigma1 <- mleList[[m]]$pars[1]
    sigmaA <- mleList[[m]]$pars[2]
    alpha <- mleList[[m]]$pars[3]
    A <- mleList[[m]]$estList$phi$A
    ages <- 1:maxA

    # Eqn 1: sd of observed age, for assumed true age 
    sigma <- vector(length=A)
    for (b in 1:A)
    {  

        num <- 1- exp(-alpha*(b-1))
        denom <- 1- exp(-alpha*(A-1))
        sigma[b] <- sigma1 + (sigmaA - sigma1)*num/denom

    }

    #Eqn 2: Calculate age-error matrix for each age
    ageErrorMat <- matrix(nrow=maxA, ncol=maxA)

    for (b in 1:maxA)
    {

      sigma_b <- sigma[b] 
      
      # Calculate probabilities for observed ages a given true age b
      for(a in 1:maxA)
      {

      # numerator
      term1 <- ((a-b)/sigma_b)^2
      num.Xab <- exp(-.5*term1)/sqrt(2*pi*sigma_b^2)

      # denom
      term1 <- ((ages-b)/sigma_b)^2
      denom.Xab <- sum(exp(-.5*term1)/sqrt(2*pi*sigma_b^2))

      ageErrorMat[a,b] <- num.Xab/denom.Xab

      }  

    } 

    # save age error matrix
    colnames(ageErrorMat) <- ages
    filename <- paste('ageErrMats/ageErrMatCase',m,'_A',maxA,'.csv', sep='')

    if(save.csv)
    write.csv(ageErrorMat, filename, row.names=F)

    ageErrMat_m[,,m] <- ageErrorMat
  }
  return(ageErrMat_m)
} # END genAgeErrMat()

# -- plotAgeErrMat ---

# Plot prob of observed age for given true age
plotAgeErrMat <- function (cases,  ageErrMat,
						   pltPars=plotPars, obsAges=NULL)
{
  	
	trueAges <- 1:dim(ageErrMat)[1]

  if(is.null(obsAges))
  	obsAges <- 1:dim(ageErrMat)[1]

	# panelRows
	pnlRows <- ceiling(max(obsAges)/40)

	# obsAgesMat <- rbind(1:20,21:40,41:60,61:80)
	# XLIMs =rbind(c(1,30),c(10,50),c(30,70),c(50,90))
  
  obsAgesMat <- rbind(1:40,41:80)
  XLIMs <- rbind(c(1,50),c(30,90))

	par(mfrow=c(pnlRows,1), mar=c(1,1,2.5,0.2), tck=-.01, 
    cex.axis=0.8, mgp=c(1.4,0.2,0), oma=c(3,3,0,0))
	
	# Plot each panel
	for (j in 1:pnlRows)
	{

	  	tmpObsAges <- obsAges[obsAges %in% obsAgesMat[j,]]

	  	for(i in tmpObsAges)
	  	{  
			if(i==min(tmpObsAges))
			plot(x=trueAges, y=ageErrMat[,i,1], type='n',
	      		 las=1, ylim=c(0,1), ylab="", xlab="",
	      		 xlim=XLIMs[j,]) 
	  
		  	for (m in cases)
          browser()
	  		lines(x=trueAges, y=ageErrMat[,i,m],  
	  			  col=pltPars$clrs[m], lty=1)      
	  	} 
	}	


    mtext(side=1, line=1, 'Observed Age', outer=T) 
    mtext(side=2, line=1, 'Probability', outer=T)

    legend('topright', cex=0.8, lty=1, lwd=1, bty='n', 
	  col=pltPars$clrs[cases],
	  legend=pltPars$caseNames[cases])

} # END plotAgeErrMat()


# -- plotAgeErrFits() --

plotAgeErrFits <- function(cases=1:length(mleList), pltPars,
						A=120, mleList)
{
  
  # vectors for legend infor
  maxObsAge <- vector(length=length(cases), 'integer')
  maxTrueAge <- vector(length=length(cases), 'integer')
  ageType <- vector(length=length(cases), 'integer')

  for (m in cases)
  {

    sigma1 <- mle.list[[m]]$pars[1]
    sigmaA <- mle.list[[m]]$pars[2]
    alpha <- mle.list[[m]]$pars[3]
    A <- mle.list[[m]]$estList$phi$A
    
    maxTrueAge[m]= mle.list[[m]]$estList$maxTrueAge
    ageType[m]= mle.list[[m]]$estList$ageType
    maxObsAge[m] <- mle.list[[m]]$estList$phi$A

    sigma <- vector(length=A)
    for (b in 1:A)
    {  
      num <- 1- exp(-alpha*(b-1))
      denom <- 1- exp(-alpha*(A-1))
      sigma[b] <- sigma1 + (sigmaA - sigma1)*num/denom

    }

    par(mar=c(3,3,0.2,0.2), tck=-0.01, mgp=c(1.6,0.6,0))
    if(m==min(cases))
    plot(x=1:A, y=sigma, 
        ylab='Estimated Standard Deviation (yr)',
        xlab='Assumed True Age', las=1, xlim=c(1,120), 
        ylim=c(0,8), type='n')

    lines(x=1:A, y=sigma, col=pltPars$clrs[m], lwd=2, 
    	  lty=pltPars$lType[m])

  }

	abline(v=65, lty=3, col='black')
	legend('topleft', cex=1, bg='white', lwd=2,
        	col=pltPars$clrs[cases],  
        	lty=pltPars$lType[cases],
        	legend=pltPars$caseNames[cases])
}  # END plotAgeErrFits()


# -- plotAgeReadDat() --

# plots of raw data
plotAgeReadDat <- function()
{

  # Calculate mean age error and SD for each age
  smry_FinalAge <- dplyr::summarise(group_by(dat, fin_specimen_age),
                              nOtoliths = length(fin_specimen_age),
                              meanError = mean(meanFinalDif),
                              medianError = median(meanFinalDif),
                              sdError = sd(meanFinalDif),
                              seError = sd(meanFinalDif)/sqrt(nOtoliths),
                              sdR1 = sd(prim_specimen_age),
                              sdR2 = sd(sec_specimen_age),
                              sdAvg = (sdR1+sdR2)/2
                              )

  write.csv(smry_FinalAge,'ageErrorStats_FinalAge.csv', row.names=F)

  # Calculate mean age error and SD for each age
  smry_ReaderAge <- dplyr::summarise(group_by(dat, meanReaderAge),
                              nOtoliths = length(meanReaderAge),
                              meanError = mean(readerDif),
                              medianError = median(readerDif),
                              sdError = sd(readerDif),
                              seError = sd(readerDif)/sqrt(nOtoliths),
                              sdR1 = sd(prim_specimen_age),
                              sdR2 = sd(sec_specimen_age),
                              sdAvg = (sdR1+sdR2)/2
                              )

  write.csv(smry_ReaderAge,'ageErrorStats_ReaderAge.csv', row.names=F)

  # Plot 1
  plot(x=smry_FinalAge$fin_specimen_age, 
       y=smry_FinalAge$fin_specimen_age+smry_FinalAge$meanError,
       xlab= 'Final Accepted Age', 
       ylab='Final age with mean ageing error +/-2SE')
  # points(x=smry_FinalAge$fin_specimen_age, y=smry_FinalAge$fin_specimen_age,
  #      col='red', pch=20)
  lines(x=smry_FinalAge$fin_specimen_age, y=smry_FinalAge$fin_specimen_age,
       col='red', lty=1)
  segments(x0=smry_FinalAge$fin_specimen_age, 
        y1= smry_FinalAge$fin_specimen_age+smry_FinalAge$meanError + 2*smry_FinalAge$seError,
        y0= smry_FinalAge$fin_specimen_age+smry_FinalAge$meanError - 2*smry_FinalAge$seError)

  quartz.save('figs/errorAges.png', type='png')


  # Compare SDs
  par(mfrow=c(1,1), mar=c(3,3,0.5,0.2), tck=-.01,
      mgp=c(1.6,0.6,0))
  # All ages
  plot(x=smry_FinalAge$fin_specimen_age, y=smry_FinalAge$sdAvg, las=1,
       xlab= 'Accepted Age', ylab='Avg SD from readings', col='black')
  points(x=smry_ReaderAge$meanReaderAge, y=smry_ReaderAge$sdAvg, col='red')
  legend('topright',col=c('black','red'), pch=1, bty='n',
        legend=c('True Age = Final Age',
                 'True Age = Mean Reader Age'))

  quartz.save('figs/SDvsAge.png', type='png')

  graphics.off()

  # Plot 3
  plot(x=smry_FinalAge$fin_specimen_age, y=smry_FinalAge$meanError, ylim=c(-15,10),
       xlab= 'Final Accepted Age', ylab='Mean ageing error +/- 2SE')
  segments(x0=smry_FinalAge$fin_specimen_age, 
        y1= smry_FinalAge$meanError + 2*smry_FinalAge$seError,
        y0= smry_FinalAge$meanError - 2*smry_FinalAge$seError)

  quartz.save('figs/errorAges2.png', type='png')

} #END plotAgeReadDat()











