# compare series A and series B used in 2014 assessment to assess if there is statistical evidence of a difference in cpue calculated using first 20 hooks or all 100 hooks

rm(list = ls())

library(car)
library(emmeans)
library(dplyr)
library(here)
library(rcompanion)

# source functions for calcultating iphc index
source('endFxFuns.r')

setwd(here('iphcEndEffect'))

# load in iphc station summary
stn <- read.csv("../tables/iphcStnSummary.csv")

# ye -  YE #s for first 20 hooks (N_it20) and all hooks (N_it)
ye <-  readRDS("../data/survey/yelloweye-rockfish.rds")
ye <- ye$set_counts
ye <- as.data.frame(ye)

# join up station info & add unique set ID
stn$station <- as.character(stn$station)
ye <- dplyr::left_join(ye, stn)
ye$setID <- paste(ye$year, ye$station, sep='_stn' )


# --- non-zero CPUE fit lm1: logCpue ~ yearF + hkCount + area + station ---

# create dataset without zero catch on first 20 hooks for analysis
ye20Nz <- procIphcYE(CPUE20=FALSE, nzCPUE=TRUE)

# Get raw CPUE smry by area
smry <- dplyr::summarize(group_by(ye20Nz, hkCount, mainIdx),
							n		= length(cpue),
							mean    = mean(cpue),
							median  = median(cpue),
							sd  	= sd(cpue),
							se      = sd(cpue)/sqrt(n))

# CPUE20 is approx 10% higher in North and 8% higher in South

# make sure year, station and area are factors
ye20Nz$yearF   <- as.factor(ye20Nz$year)
ye20Nz$hkCount <- as.factor(ye20Nz$hkCount)
ye20Nz$station <- as.factor(ye20Nz$station)

# Complet analysis on log cpue
ye20Nz$logCpue <- log(ye20Nz$cpue)

m1 <- lm(logCpue ~ yearF + hkCount + mainIdx + station, ye20Nz)
anova(m1)

# Check normality - evidence this is not normal
par(mfrow=c(1,2))
plotNormalHistogram(ye20Nz$logCpue,breaks=60)
shapiro.test(residuals(m1))
qqnorm(residuals(m1))
qqline(residuals(m1))

u.ls <- CLD(lsmeans(m1,~ hkCount + mainIdx))
u.ls$meanCpue <- exp(u.ls$lsmean)

# Estimated 0.81 bias correction
bias  <- exp(u.ls$lsmean[3])/exp(u.ls$lsmean[4])

# Check using only south and north data

# South
nzSouth <- subset(ye20Nz, mainIdx=='southIdx')
plotNormalHistogram(nzSouth$logCpue,breaks=60)
mSouth <- lm(logCpue ~ yearF + hkCount + station, nzSouth)
anova(mSouth)

plotNormalHistogram(nzNorth$logCpue,breaks=60)
shapiro.test(residuals(mNorth))
qqnorm(residuals(mNorth))
qqline(residuals(mNorth))

u.South <- CLD(lsmeans(mSouth,~ hkCount))
exp(u.South$lsmean[1])/exp(u.South$lsmean[2]) #0.79

# North
nzNorth <- subset(ye20Nz, mainIdx=='northIdx')
mNorth <- lm(logCpue ~ yearF + hkCount + station, nzNorth)
anova(mNorth)

plotNormalHistogram(nzNorth$logCpue,breaks=60)
shapiro.test(residuals(mNorth))
qqnorm(residuals(mNorth))
qqline(residuals(mNorth))

u.North <- CLD(lsmeans(mNorth,~ hkCount))
exp(u.North$lsmean[1])/exp(u.North$lsmean[2]) #0.82



# --- non-zero index dat fit lm: cpue ~ yearF + hkCount + area ---

m2 <- lm(logCpue ~ yearF + hkCount + mainIdx, ye20Nz)
anova(m2)
car::Anova(m2, type='III')

u.m2 <- CLD(lsmeans(m2,~ hkCount + mainIdx))
c(exp(u.m2$lsmean[3])/exp(u.m2$lsmean[4]),
  exp(u.m2$lsmean[1])/exp(u.m2$lsmean[2]))
# bias correction remains the same with or without station effect



# --- non-zero CPUE fit lm2: tukeyCpue ~ yearF + hkCount + area + station ---

# Complet analysis on tukey transformed cpue
ye20Nz$tkCpue  <- transformTukey(ye20Nz$cpue)
lambda <- transformTukey(ye20Nz$cpue, returnLambda=TRUE)

m.tk <- lm(tkCpue ~ yearF + hkCount + mainIdx + station, ye20Nz)
anova(m.tk)

# Check normality
par(mfrow=c(1,2))
plotNormalHistogram(ye20Nz$tkCpue,breaks=60) # this looks slightly better better
shapiro.test(residuals(m.tk))
qqnorm(residuals(m.tk))
qqline(residuals(m.tk))

u.tk <- CLD(lsmeans(m.tk,~ hkCount + mainIdx))

# Calculate bias correction for North and South non-zero sets with first 20 hooks)
u.tk$meanCpue <- u.tk$lsmean^(1/lambda)

# Estimated as 0.817 in North and 0.814 in South, similar to log scale
biasCorrTk_p <- c(u.tk$meanCpue[3]/u.tk$meanCpue[4], 
				  u.tk$meanCpue[1]/u.tk$meanCpue[2])



# --- non-zero index fit lm: nzIdx ~ yearF + hkCount + area ---

idxNz <- createIdx(nzCPUE=TRUE,  CPUE20=FALSE)
idxNz$yearF <- as.factor(idxNz$year)
idxNz$hkCount <- as.factor(idxNz$hkCount)

m.idxNz <- lm(meanCPUE ~ yearF + hkCount + mainIdx, idxNz)
anova(m.idxNz)
car::Anova(m.idxNz, type='III') # p =0.04

# Check normality - passes normality test
plotNormalHistogram(idxNz$meanCPUE,breaks=60)
shapiro.test(residuals(m.idxNz))
qqnorm(residuals(m.idxNz))
qqline(residuals(m.idxNz))

u.idxNz <- CLD(lsmeans(m.idxNz,~ hkCount + mainIdx))

# this comes out pretty close to raw data summary
smry

# bias correctio
u.idxNz$lsmean[3]/u.idxNz$lsmean[4] # 0.93 in North
u.idxNz$lsmean[1]/u.idxNz$lsmean[2] # 0.88 in South

#try log-scale
m.idxNz <- lm(log(meanCPUE) ~ yearF + hkCount + mainIdx, idxNz)
anova(m.idxNz)
car::Anova(m.idxNz, type='III') # p =0.02

# Check normality - passes normality test
plotNormalHistogram(log(idxNz$meanCPUE),breaks=60)
shapiro.test(residuals(m.idxNz))
qqnorm(residuals(m.idxNz))
qqline(residuals(m.idxNz))

# try tukey scale
idxNz$tkCpue  <- transformTukey(idxNz$meanCPUE)
plotNormalHistogram(idxNz$tkCpue,breaks=60)





# --- index fit including zero sets: Idx ~ yearF + hkCount + area ---

idx <- createIdx(nzCPUE=FALSE)
idx$yearF <- as.factor(idx$year)
idx$hkCount <- as.factor(idx$hkCount)

m.idx <- lm(meanCPUE ~ yearF + hkCount + mainIdx, idx)
anova(m.idx)
car::Anova(m.idx, type='III') # p =0.65

# Check normality - passes normality test
plotNormalHistogram(idx$meanCPUE,breaks=60)
shapiro.test(residuals(m.idx))
qqnorm(residuals(m.idx))
qqline(residuals(m.idx))

u.idx <- CLD(lsmeans(m.idx,~ hkCount + mainIdx))


# bias correc
u.idx$lsmean[3]/u.idx$lsmean[4] # 0.983 in North
u.idx$lsmean[1]/u.idx$lsmean[2] # 0.968 in South

# Caculate t-test for ratio of means
idxBias <-  dplyr::select(idx, mainIdx:hkCount, meanCPUE) %>%
			tidyr::spread('hkCount', meanCPUE)
idxBias$biasC_it20 <- idxBias$C_it20/idxBias$C_it
idxBias$yearF <- as.factor(idxBias$year)	

m.bias <- lm(biasC_it20 ~ yearF + mainIdx, idxBias)
CLD(lsmeans(m.bias,~ mainIdx))

#try log-scale
m.idx <- lm(log(meanCPUE) ~ yearF + hkCount + mainIdx, idx)
anova(m.idx)
car::Anova(m.idx, type='III') # p =0.9


# --- PLOTS: unadjusted nzIdx and idx for North/South ---

# Station Threshold 1: at least 1 year with YE catch
idxNz 	<- createIdx(nzCPUE=TRUE, CPUE20=TRUE)
idx 	<- createIdx(nzCPUE=FALSE, CPUE20=TRUE)
idxBias <- calcIdxBias(idx)

# merge idx and idxNz
plotDat <- list(idxNz 	= idxNz,
				idx 	= idx,
				idxBias = idxBias)

# Plot unadjusted indices
pdf('idx20vs100_thresh1.pdf')
idxPlot(plotDat)
dev.off()


# Station Threshold 11: at least 11 year with YE catch
idxNz 	<- createIdx(nzCPUE=TRUE, CPUE20=TRUE, thresh=11)
idx 	<- createIdx(nzCPUE=FALSE, CPUE20=TRUE, thresh=11)
idxBias <- calcIdxBias(idx)

# merge idx and idxNz
plotDat <- list(idxNz 	= idxNz,
				idx 	= idx,
				idxBias = idxBias)

# Plot unadjusted indices
pdf('idx20vs100_thresh11.pdf')
idxPlot(plotDat)
dev.off()

# set wd back to yeopmod
setwd(here())

